(function() {
	function reminderService($http, localStorageService) {
		this.getDueBillReminders = function(callback) {
			$http.get("reminders/dueBillReminders")
			.success(function(data, status) {
				callback(data);
			})
			.error(function() {
				alert("Some error occurred!!!");
			})
		}

		this.updateNextReminder = function(paramBillReminder, callback) {
            $http.post("reminders/updateNextReminder", paramBillReminder)
            .success(function(data, status) {
                callback(data);
            })
            .error(function() {
                alert("Some error occurred!!!");
            })
        }
	}
	
	helium.service("reminderService", reminderService);
})();