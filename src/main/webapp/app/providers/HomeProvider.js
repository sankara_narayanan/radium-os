(function() {
	function homeService($http) {
		this.getDashboardData = function(callback) {
			$http.get("getDashboardData")
			.success(function(data, status) {
				callback(data);
			})
			.error(function(data, status) {
				alert("Some error occurred!!!");
			})
		}
	}
	
	helium.service("homeService", homeService);
})();