(function() {
	function fundProvider($http, localStorageService) {
		this.listTransactions = function(callback) {
			$http.get("funds/list")
			.success(function(data, status) {
				callback(data);
			})
			.error(function() {
				alert("Some error occurred!!!");
			})
		}
		
		this.saveTransaction = function(paramTransaction, callback) {
			var transaction = {};
			for (var key in paramTransaction) {
				transaction[key] = paramTransaction[key];
			}
			transaction.date = transaction.date.getTime() / 1000;

			$http.post("funds/save", transaction)
			.success(function(data, status) {
				localStorageService.put("status", "success");
				localStorageService.put("message", "Fund transaction saved successfully!!!");
				callback(data);
			})
			.error(function(data, status) {
				localStorageService.put("status", "error");
				localStorageService.put("message", "Some error occurred!!!");
				alert("Some error occurred!!!");
			});			
		}

		this.getNav = function(fund, date, callback) {
		    date = date.getTime();

		    $http.get("funds/nav", {
		        params: {
		            fund: fund,
		            date: date
		        }
		    })
			.success(function(data, status) {
				alert(data);
				callback(data);
			})
			.error(function() {
				alert("Some error occurred!!!");
			})
		}

		this.getAllFundsValue = function(callback) {
		    $http.get("funds/getAllFundsValue")
			.success(function(data, status) {
				callback(data);
			})
			.error(function() {
				alert("Some error occurred!!!");
			})
		}

        this.viewFund = function(fundId, callback) {
		    $http.get("funds/view/" + fundId)
			.success(function(data, status) {
				callback(data);
			})
			.error(function() {
				alert("Some error occurred!!!");
			})

        }

		this.listFunds = function(callback) {
		    $http.get("funds/listFunds")
			.success(function(data, status) {
				callback(data);
			})
			.error(function() {
				alert("Some error occurred!!!");
			})
		}

		/*this.updateRun = function(paramRun, callback) {
			var run = {};
			for (var key in paramRun) {
				run[key] = paramRun[key];
			}
			run.runDate = run.runDate.getTime();

			$http.post("run/update", run)
			.success(function(data, status) {
				localStorageService.put("status", "success");
				localStorageService.put("message", "Run updated successfully!!!");
				callback(data);
			})
			.error(function(data, status) {
				localStorageService.put("status", "error");
				localStorageService.put("message", "Some error occurred!!!");
				callback(data);
			});			
		}		
		
		this.deleteRun = function(run, callback) {
			$http.post("run/delete", run)
			.success(function(data, status) {
				localStorageService.put("status", "success");
				localStorageService.put("message", "Run deleted successfully!!!");
				callback(data);				
			})
			.error(function(data, status) {
				localStorageService.put("status", "error");
				localStorageService.put("message", "Some error occurred!!!");
				callback(data);
			})
		}*/
	}
	
	helium.service("fundProvider", fundProvider);
})();