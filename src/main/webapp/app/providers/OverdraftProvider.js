(function() {
	function overdraftProvider($http) {
		this.getDetails = function(callback) {
			$http.get("overdraft/getDetails")
			.success(function(data, status) {
				callback(data);
			})
			.error(function() {
				alert("Some error occurred!!!");
			})
		}

		this.transact = function(paramTransact, callback) {
			var transact = {};
			for (var key in paramTransact) {
				transact[key] = paramTransact[key];
			}
			transact.date = transact.date.getTime();

            $http.post("overdraft/transact", transact)
            .success(function(data, status) {
                callback(data);
            })
            .error(function() {
                alert("Some error occurred!!!");
            })
        }

		this.changeLimit = function(paramLimit, callback) {
			var limit = {};
			for (var key in paramLimit) {
				limit[key] = paramLimit[key];
			}
			limit.date = limit.date.getTime();

            $http.post("overdraft/changeLimit", limit)
            .success(function(data, status) {
                callback(data);
            })
            .error(function() {
                alert("Some error occurred!!!");
            })
        }
	}

	helium.service("overdraftProvider", overdraftProvider);
})();