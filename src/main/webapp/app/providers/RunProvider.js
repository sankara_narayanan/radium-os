(function() {
	function runProvider($http, localStorageService) {
		this.listRuns = function(startTime, endTime, callback) {
			$http.get("run/list", {
				params: {
					startTime: startTime.getTime(),
					endTime: endTime.getTime()
				}
			})
			.success(function(data, status) {
				callback(data);
			})
			.error(function() {
				alert("Some error occurred!!!");
			})
		}
		
		this.saveRun = function(paramRun, callback) {
			var run = {};
			for (var key in paramRun) {
				run[key] = paramRun[key];
			}
			run.runDate = run.runDate.getTime();

			$http.post("run/save", run)
			.success(function(data, status) {
				localStorageService.put("status", "success");
				localStorageService.put("message", "Run saved successfully!!!");
				callback(data);
			})
			.error(function(data, status) {
				localStorageService.put("status", "error");
				localStorageService.put("message", "Some error occurred!!!");
				callback(data);
			});			
		}
		
		this.updateRun = function(paramRun, callback) {
			var run = {};
			for (var key in paramRun) {
				run[key] = paramRun[key];
			}
			run.runDate = run.runDate.getTime();

			$http.post("run/update", run)
			.success(function(data, status) {
				localStorageService.put("status", "success");
				localStorageService.put("message", "Run updated successfully!!!");
				callback(data);
			})
			.error(function(data, status) {
				localStorageService.put("status", "error");
				localStorageService.put("message", "Some error occurred!!!");
				callback(data);
			});			
		}		
		
		this.deleteRun = function(run, callback) {
			$http.post("run/delete", run)
			.success(function(data, status) {
				localStorageService.put("status", "success");
				localStorageService.put("message", "Run deleted successfully!!!");
				callback(data);				
			})
			.error(function(data, status) {
				localStorageService.put("status", "error");
				localStorageService.put("message", "Some error occurred!!!");
				callback(data);
			})
		}
	}
	
	helium.service("runProvider", runProvider);
})();