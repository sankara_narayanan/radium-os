(function() {
	function expenseService($http, localStorageService) {
		this.saveExpense = function(paramExpense, callback) {
			var expense = {};
			for (var key in paramExpense) {
				expense[key] = paramExpense[key];
			}
			expense.expenseDate = expense.expenseDate.getTime();
			$http.post("expense/save", expense)
			.success(function(data, status) {
				localStorageService.put("status", "success");
				localStorageService.put("message", "Expense saved successully!!!");
				callback(data);
			})
			.error(function() {
				localStorageService.put("status", "failure");
				localStorageService.put("message", "Some error occurred!!!");
				alert("Some error occured!!!");
			})
		}
		
		this.updateExpense = function(paramExpense, callback) {
			var expense = {};
			for (var key in paramExpense) {
				expense[key] = paramExpense[key];
			}
			expense.expenseDate = expense.expenseDate.getTime();
			$http.post("expense/update", expense)
			.success(function(data, status) {
				localStorageService.put("status", "success");
				localStorageService.put("message", "Expense updated successully!!!");
				callback(data)
			})
			.error(function() {
				localStorageService.put("status", "failure");
				localStorageService.put("message", "Some error occurred!!!");
				alert("Some error occurred!!!");
			})
		}
		
		this.listExpenses = function(startTime, endTime, callback) {
			$http.get("expense/list", {
				params: {
					startTime: startTime.getTime(),
					endTime: endTime.getTime()
				}
			})
			.success(function(data, status) {
				callback(data);
			})
			.error(function() {
				alert("Some error occurred!!!");
			})
		}

		this.getMonthlyExpenseList = function(date, callback) {
			date = date.getTime();

			$http.get("expense/getMonthlyExpenseList", {
				params: {
					date: date
				}
			})
			.success(function(data, status) {
				callback(data);
			})
			.error(function() {
				alert("Some error occurred!!!");
			})
		}

		this.deleteExpense = function(expense, callback) {
			$http.post("expense/delete", expense)
			.success(function(data, status) {
				callback(data);
			})
			.error(function() {
				alert("Some error occurred!!!");
			})
		}
		
		this.getMonthlyReportByItem = function(startTime, endTime, callback) {
			$http.get("expense/getMonthlyReportByItem", {
				params: {
					startTime: startTime.getTime(),
					endTime: endTime.getTime()
				}
			})
			.success(function(data, status) {
				callback(data);
			})
			.error(function(data, status) {
				alert("Some error occurred!!!");
			})
		}

		this.getCategoryItemMapping = function(callback) {
			$http.get("expense/getCategoryItemMapping")
			.success(function(data, status) {
				callback(data);
			})
			.error(function(data, status) {
				alert("Some error occurred!!!");
			})
		}
	}
	
	helium.service("expenseService", expenseService);
})();