(function() {
	function habitProvider($http, localStorageService) {
		this.listHabits = function(callback) {
			$http.get("habits/list")
			.success(function(data, status) {
				callback(data);
			})
			.error(function() {
				alert("Some error occurred!!!");
			})
		}

		this.getMonthLog = function(habitId, date, callback) {
		    date = date.getTime();
		    $http.get("habits/monthLog", {
		        params: {
		            habitId: habitId,
		            date: date
		        }
		    })
			.success(function(data, status) {
				callback(data);
			})
			.error(function() {
				alert("Some error occurred!!!");
			})
		}

        this.logHabit = function(habitId, date, isCompleted, callback) {
		    date = date.getTime() / 1000;

		    $http.get("habits/log", {
		        params: {
		            habitId: habitId,
		            date: date,
		            isCompleted: isCompleted
		        }
		    })
			.success(function(data, status) {
				callback(data);
			})
			.error(function() {
				alert("Some error occurred!!!");
			})
	    }

		this.saveHabit = function(paramHabit, callback) {
			var habit = {};
			for (var key in paramHabit) {
				habit[key] = paramHabit[key];
			}

			$http.post("habits/save", habit)
			.success(function(data, status) {
				callback(data);
			})
			.error(function(data, status) {
				alert("Some error occurred!!!");
			});			
		}
	}
	
	helium.service("habitProvider", habitProvider);
})();