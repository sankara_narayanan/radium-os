(function() {
	function localStorageService() {
		var cache = {};
		
		this.put = function(key, value) {
			cache[key] = value;
		}
		
		this.get = function(key) {
			if (cache[key] != null) {
				var value = cache[key];
				delete cache[key];
				return value;
			}
		}
	}
	
	helium.service("localStorageService", localStorageService);
})();