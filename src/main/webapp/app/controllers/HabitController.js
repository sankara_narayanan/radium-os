(function() {
	function habitController($scope, $location, $route, habitProvider, localStorageService) {
        $scope.listHabits = function() {
            habitProvider.listHabits(function(data) {
                $scope.habits = data;

                if (data.length > 0) {
                    $scope.activeHabit = data[0];
                    $scope.activeDate = new Date();
                    $scope.getMonthLog($scope.activeHabit, $scope.activeDate);
                }
            })
        }

        $scope.getMonthLog = function(habit, date) {
            habitProvider.getMonthLog(habit.id, date, function(data) {
                $scope.daysCompleted = data.daysCompleted;
                $scope.daysUncompleted = data.daysUncompleted;
                $scope.refreshDaysPending(date, data.daysPending);
            })
        }

        $scope.refreshDaysPending = function(paramDate, daysPending) {
            $scope.datesPending = [];
            for (var index = 0; index < daysPending.length; index++) {
                var date = new Date();
                date.setTime(paramDate.getTime());
                date.setDate(daysPending[index]);
                $scope.datesPending.push(date);
            }
        }

        $scope.changeHabit = function(habit) {
            $scope.activeHabit = habit;
            $scope.getMonthLog($scope.activeHabit, $scope.activeDate);
        }

        $scope.addNewHabit = function() {
            $location.path("habits/add");
        }

        $scope.log = function(isCompleted, date) {
            date = new Date(date.getFullYear(), date.getMonth(), date.getDate());
            habitProvider.logHabit($scope.activeHabit.id, date, isCompleted, function(data){
                $scope.getMonthLog($scope.activeHabit, date);
            })
        }

        $scope.refreshCalendar = function(paramDate) {
            //var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
            //var days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

            var date = new Date(paramDate);
            var monthIndex = date.getMonth();

            $scope.month = $scope.months[date.getMonth()];
            $scope.year = date.getFullYear();

            $scope.calendar = [];
            for (var row = 0; monthIndex == date.getMonth(); row++) {
                $scope.calendar[row] = [];
                for (var col = 0; col < 7; col++) {
                    if (date.getDay() == col && monthIndex == date.getMonth()) {
                        $scope.calendar[row][col] = date.getDate();
                        date.setDate(date.getDate() + 1);
                    }
                    else {
                        $scope.calendar[row][col] = col + 100;
                    }
                }
            }

            if ($scope.activeHabit != null) {
                $scope.getMonthLog($scope.activeHabit, paramDate);
            }
        }

        $scope.prev = function() {
            $scope.index -= 1;

            var date = new Date();
            date.setDate(1);
            date.setMonth(date.getMonth() + $scope.index);

            $scope.activeDate = date;
            $scope.refreshCalendar(date);
        }

        $scope.next = function() {
            $scope.index += 1;

            var date = new Date();
            date.setDate(1);
            date.setMonth(date.getMonth() + $scope.index);

            $scope.activeDate = date;
            $scope.refreshCalendar(date);
        }

        var today = new Date();
        today.setDate(1);

        $scope.index = 0;
        $scope.months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        $scope.days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
        $scope.colors = {'#E1B14A':'stop', '#F98866': 'stop', '#FF420E': 'stop', '#80BD9E':'stop', '#89DA59':'stop', '#336B87':'stop', '#4CB5F5':'stop', '#F52549': 'stop'}
        $scope.refreshCalendar(today);

        $scope.selectColor = function(paramKey) {
            for (var key in $scope.colors) {
                $scope.colors[key] = 'stop';
            }
            $scope.colors[paramKey] = 'check';
            $scope.habit.color = paramKey;
        }

        $scope.saveHabit = function() {
            var day = '';
            for (var index in $scope.proxyDays) {
                day += $scope.proxyDays[index] + ',';
            }

            if ($scope.habit.name === '' || day === '') {
                return;
            }

            $scope.habit.days = day.substring(0, day.length - 1);

            habitProvider.saveHabit($scope.habit, function(data) {
                $location.path("habits/monthLog");
            })
        }

        $scope.toggleDay = function(day) {
            var idx = $scope.proxyDays.indexOf(day);

            // Is currently selected
            if (idx > -1) {
              $scope.proxyDays.splice(idx, 1);
            }
            // Is newly selected
            else {
              $scope.proxyDays.push(day);
            }
        }

		switch ($location.path()) {
			case "/habits/monthLog":
				$scope.listHabits();
				break;
			case "/habits/add":
			    $scope.habit = {name:'', days:'', color:''};
			    $scope.proxyDays = []
			    break;
		}

    }
	
	helium.controller("HabitController", habitController);
})();