(function() {
	function runController($scope, $location, $route, runProvider, localStorageService) {
		var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
		
		$scope.listRuns = function(startDate) {
			//year, month, date, hour, minute, seconds
			var startTime = new Date(startDate.getFullYear(), startDate.getMonth(), 1, 0, 0, 0, 0);
			var endTime = new Date(startDate.getFullYear(), startDate.getMonth() + 1, 0, 23, 59, 59, 999);
		
			runProvider.listRuns(startTime, endTime, function(data) {
				for (var index = 0; index < data.length; index++) {
					var run = data[index];
					
					var runDate = new Date(run.runDate);
					var date = runDate.getDate();
					var month = months[runDate.getMonth()];
					var year = runDate.getFullYear();
					run.runDate = date + " " + month + " " + year;
					
					var duration = run.duration;
					var hh = Math.trunc(duration / 3600);
					var mm = Math.trunc((duration % 3600) / 60);
					var ss = (duration % 3600) % 60;
					run.duration = hh + ":" + mm + ":" + ss;
				}
				$scope.runs = data;
			})
		}
		
		$scope.saveRun = function(run) {
			run.duration = (run.hh * 60 * 60) + (run.mm * 60) + run.ss;
			runProvider.saveRun(run, function(data) {
				$location.path("listRuns");
			})
		}
		
		$scope.updateRun = function(run) {
			run.duration = (run.hh * 60 * 60) + (run.mm * 60) + run.ss;
			runProvider.updateRun(run, function(data) {
				$location.path("listRuns");
			})
		}		
		
		$scope.deleteRun = function(paramRun) {
			var run = {};
			run.id = paramRun.id;
			runProvider.deleteRun(run, function(data) {
				$route.reload();
			})
		}
		
		$scope.addRun = function() {
			$location.path("/addRun");
		}
		
		$scope.editRun = function(paramRun) {
			var run = {};
			run.id = paramRun.id;
			run.distance = paramRun.distance;
			
			run.hh = parseInt(paramRun.duration.split(":")[0]);
			run.mm = parseInt(paramRun.duration.split(":")[1]);
			run.ss = parseInt(paramRun.duration.split(":")[2]);
			
			var date = paramRun.runDate.split(" ")[0];
			var month = months.indexOf(paramRun.runDate.split(" ")[1]);
			var year = paramRun.runDate.split(" ")[2];
			run.runDate = new Date(year, month, date); 
			
			localStorageService.put("runToBeEdited", run);
			$location.path("/editRun");
		}
		
		$scope.viewRuns = function() {
			$location.path("/listRuns");
		}
		
		$scope.status = localStorageService.get('status');
		$scope.message = localStorageService.get('message'); 
		
		switch ($location.path()) {
			case "/listRuns":
				$scope.listRuns(new Date());
				break;
			case "/addRun":
				$scope.run = {};
				$scope.run.runDate = new Date();
				break;
			case "/editRun":
				$scope.run = localStorageService.get("runToBeEdited");
				break;
		}
	}
	
	helium.controller("RunController", runController);
})();