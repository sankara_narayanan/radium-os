(function() {
	function fundController($scope, $location, $route, $filter, fundProvider, localStorageService) {

        $scope.showPortfolio = function() {
            fundProvider.getAllFundsValue(function(data) {
                $scope.portfolio = data;
            });
        }

		$scope.listFunds = function() {
		    fundProvider.listFunds(function(data) {
                $scope.fundViewList = data;
		    })
		}

        $scope.gotoFund = function(fundCode) {
			localStorageService.put('fundCode', fundCode);
			$location.path("/funds/view");
        }

		$scope.viewFund = function(id) {
		    fundProvider.viewFund(id, function(data) {
		        $scope.fund = data;
		    })
		}

		$scope.saveTransaction = function() {
		    $scope.transaction.quantity = parseFloat($filter('number')($scope.transaction.purchaseValue / $scope.transaction.nav, 4));

			fundProvider.saveTransaction($scope.transaction, function(data) {
				alert("Fund transaction saved successfully");
				$location.path("/funds/showPortfolio");
			})
		}

		$scope.getNav = function() {
		    var fund = $scope.transaction.fund;
		    var date = $scope.transaction.date;
		    if (fund != null && date != null) {
		        fundProvider.getNav(fund, date, function(data) {
		            //var fundNav = data;
		            //$scope.transaction.nav = fundNav.nav;
		            alert(data);
		            $scope.transaction.nav = data;
		        })
		    }
		}

		$scope.addTransaction = function(fundCode) {
			localStorageService.put('fundCode', fundCode);
			$location.path("/funds/addTransaction");
		}

		$scope.viewFunds = function() {
			$location.path("/funds/list");
		}

		$scope.tableRowLimit = 5;
		$scope.toggleCollapse = function() {
		    $scope.tableRowLimit = ($scope.tableRowLimit != 5) ? 5 : $scope.fund.transactions.length;
		}
		
		$scope.status = localStorageService.get('status');
		$scope.message = localStorageService.get('message'); 
		
		switch ($location.path()) {
			case "/funds/showPortfolio":
				$scope.showPortfolio();
				break;
			case "/funds/list":
				$scope.listFunds();
				break;
			case "/funds/view":
				$scope.viewFund(localStorageService.get('fundCode'));
				break;
			case "/funds/addTransaction":
				$scope.transaction = new Object();
				$scope.transaction.fund = localStorageService.get('fundCode').toString();
				break;
			case "/editRun":
				//$scope.run = localStorageService.get("runToBeEdited");
				break;
		}
	}
	
	helium.controller("FundController", fundController);
})();