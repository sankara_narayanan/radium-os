(function() {
	function overdraftController($scope, $location, overdraftProvider) {
		$scope.getDetails = function() {
			overdraftProvider.getDetails(function(data) {
				$scope.odAcc = data;
			})
		}

		$scope.gotoTransact = function() {
		    $location.path("/overdraft/transact");
		}

		$scope.transact = function() {
		    overdraftProvider.transact($scope.transaction, function() {
		        $location.path("/overdraft/getDetails");
		    })
		}

		$scope.gotoChangeLimit = function() {
		    $location.path("/overdraft/changeLimit");
		}

		$scope.changeLimit = function() {
		    overdraftProvider.changeLimit($scope.limit, function() {
		        $location.path("/overdraft/getDetails");
		    })
		}

        $scope.date = new Date();

		switch ($location.path()) {
			case "/overdraft/getDetails":
				$scope.getDetails();
				break;
			case "/overdraft/transact":
			    $scope.transaction = {};
			    $scope.transaction.date = new Date();
			    break;
			case "/overdraft/changeLimit":
			    $scope.limit = {};
			    $scope.limit.date = new Date();
			    break;
		}
	}
	
	helium.controller("OverdraftController", overdraftController);
})();