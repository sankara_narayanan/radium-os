(function() {
	function expenseController($scope, $location, $route, $filter, $timeout, expenseService, reminderService, localStorageService) {
		$scope.saveExpense = function(expense) {
			expenseService.saveExpense(expense, function(expenseRes) {
				if (expenseRes.id != null) {
					alert("Expense saved successfully!!");

					if ($scope.billToBePaid != null && !$scope.billToBePaid.paid) {
					    var bill = $scope.billToBePaid.bill;
					    if (bill.category == expenseRes.category && bill.item == expenseRes.item) {
					        reminderService.updateNextReminder(bill, function(data) {
					            $location.path("/home");
					        });
					    }
					} else {
					    $location.path("/showExpense");
					}
				}
			})
		}

		$scope.updateExpense = function(expense) {
			expenseService.updateExpense(expense, function(expenseRes) {
				alert("Expense updated successfully!!");
				$location.path("/showExpense");
			})
		}
		
		$scope.listExpenses = function(startDate) {
			var startTime = new Date(startDate.getTime());
			startTime.setHours(0, 0, 0, 0);
			var endTime = new Date(startDate.getTime());
			endTime.setHours(23, 59, 59, 999);
			
			expenseService.listExpenses(startTime, endTime, function(listExpensesRes) {
				$scope.expenses = listExpensesRes;
			})
		}

        $scope.showExpense = function(paramDate) {
            expenseService.getMonthlyExpenseList(paramDate, function(data) {
                // Hide expense list by default
                var expenseListDiv = document.getElementById('expense-list');
                expenseListDiv.className = 'hide';

                $scope.expense = data;
                $scope.amountSpent = 0;
                for (var date in data) {
                    $scope.amountSpent += data[date];
                }
                $scope.showExpensePie();
            })
        }

        $scope.listExpenseByDate = function(date, rowIndex, colIndex) {
            $scope.resetExpenseCalendar();

            // Collapse expense calendar & highlight selected date
            var expenseWeekList = document.getElementById('expense-calendar').rows;
            for (var index = 1; index < expenseWeekList.length - 1; index++) {
                if (rowIndex != index - 1)
                    expenseWeekList[index].className = 'hide';
            }
            expenseWeekList[rowIndex + 1].cells[colIndex].style.background = '#1995AD';
            expenseWeekList[rowIndex + 1].cells[colIndex].style.borderRadius = '10%';
            expenseWeekList[rowIndex + 1].cells[colIndex].style.color = '#FFFFFF';

            // Show expense list by date
            var expenseListDiv = document.getElementById('expense-list');
            expenseListDiv.className = 'show';
            $scope.showExpenseListByDate = true;

            $scope.listExpenses(date);
        }

        $scope.hideExpenseList = function() {
            $scope.resetExpenseCalendar();

            // Hide expense list by date
            var expenseListDiv = document.getElementById('expense-list');
            expenseListDiv.className = 'hide';
            $scope.showExpenseListByDate = false;
        }

        $scope.resetExpenseCalendar = function() {
            // Reset expense calendar
            var expenseWeekList = document.getElementById('expense-calendar').rows;
            for (var row = 1; row < expenseWeekList.length - 1; row++) {
                expenseWeekList[row].className = '';
                for (var col = 0; col < 7; col++) {
                    expenseWeekList[row].cells[col].style.background = '';
                    expenseWeekList[row].cells[col].style.color = '';
                }
            }
        }

		$scope.showExpensePie = function() {
			var startDate = new Date();
			var startMonth = (startDate.getDate() >= 24) ? startDate.getMonth() : startDate.getMonth() - 1;

			var startTime = new Date(startDate.getFullYear(), startMonth, 24, 0, 0, 0, 0);
			var endTime = new Date(startDate.getFullYear(), startMonth + 1, 23, 23, 59, 59, 999);

			expenseService.getMonthlyReportByItem(startTime, endTime, function(data) {
                var expensesByCat = {};
                var amountSpent = 0;
                for (var index = 0; index < data.length; index++) {
                    var category = data[index].category;
                    expensesByCat[category] = (expensesByCat[category] == null) ? 0 : expensesByCat[category];
                    expensesByCat[category] += data[index].expense;

                    amountSpent += data[index].expense;
                }

                var pieData = [];
                for (var key in expensesByCat) {
                    var expense = {};
                    expense.name = key;
                    expense.y = expensesByCat[key];
                    pieData.push(expense);
                }
                expensePie(pieData);
			});
        }

		$scope.deleteExpense = function(expense) {
			expenseService.deleteExpense(expense, function(deleteExpenseRes) {
				alert("Expense deleted successfully");
				$route.reload();
			})
		}

		$scope.getCategoryItemMapping = function() {
			expenseService.getCategoryItemMapping(function(data) {
			    $scope.categoryItemMap = data;
			    if ($scope.expense.category == null) {
			        $scope.itemList = [];
			    } else {
			        $scope.itemList = $scope.categoryItemMap[$scope.expense.category];
			    }
			})
		}

		$scope.filterItems = function() {
		    $scope.itemList = $scope.categoryItemMap[$scope.expense.category];
		}

		$scope.addExpense = function() {
            $location.path("/addExpense");
		}
		
		$scope.editExpense = function(expense) {
			localStorageService.put('expenseToBeEdited', expense);
			$location.path("/editExpense");
		}

		$scope.viewExpenses = function() {
		    $location.path("/showExpense");
		}

		$scope.status = localStorageService.get('status');
		$scope.message = localStorageService.get('message'); 

        $scope.index = 0;
        $scope.months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        $scope.days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

        $scope.refreshCalendar = function(paramDate) {
            var date = new Date(paramDate);
            var monthIndex = date.getMonth();

            $scope.month = $scope.months[date.getMonth()];
            $scope.year = date.getFullYear();

            $scope.calendar = [];
            for (var row = 0; monthIndex == date.getMonth(); row++) {
                $scope.calendar[row] = [];
                for (var col = 0; col < 7; col++) {
                    if (date.getDay() == col && monthIndex == date.getMonth()) {
                        $scope.calendar[row][col] = new Date(date.getFullYear(), date.getMonth(), date.getDate(), 0, 0, 0, 0);
                        date.setDate(date.getDate() + 1);
                    }
                    else {
                        $scope.calendar[row][col] = col + 100;
                    }
                }
            }

            $scope.showExpense(paramDate);
        }

        $scope.prev = function() {
            $scope.index -= 1;

            var date = new Date();
            date.setDate(1);
            date.setMonth(date.getMonth() + $scope.index);

            $scope.refreshCalendar(date);
        }

        $scope.next = function() {
            $scope.index += 1;

            var date = new Date();
            date.setDate(1);
            date.setMonth(date.getMonth() + $scope.index);

            $scope.refreshCalendar(date);
        }

        $scope.switchView = function() {
            if ($scope.expenseView === 'calendar') {
			    $scope.expenseView = 'stats';
                document.getElementById('expense-report').className = '';
                document.getElementById('expense-calendar').className = 'table table-striped hide';
                document.getElementById('expense-list').className = 'hide';
            } else {
                $scope.expenseView = 'calendar';
                document.getElementById('expense-report').className = 'hide';
                document.getElementById('expense-calendar').className = 'table table-striped';
                document.getElementById('expense-list').className = 'hide';
            }
        }

        var today = new Date();
        today.setDate(1);

		switch ($location.path()) {
			case "/showExpense":
			    $scope.refreshCalendar(today);
			    $scope.expenseView = 'calendar'
			    break;
			case "/editExpense":
				var expenseToBeEdited = localStorageService.get('expenseToBeEdited')
				$scope.expense = expenseToBeEdited;
				$scope.expense.expenseDate = new Date($scope.expense.expenseDate);
				$scope.getCategoryItemMapping();
				break;
			case "/addExpense":
			    $scope.expense = {};
			    var billToBePaid = localStorageService.get('billToBePaid');
			    if (billToBePaid != null && !billToBePaid.paid) {
				    $scope.billToBePaid = billToBePaid;
				    $scope.expense = billToBePaid.bill;
			    }
				$scope.getCategoryItemMapping();
				break;
		}
	}
	
	helium.controller("ExpenseController", expenseController)
})();

function expensePie(data) {
    // Create the chart
    Highcharts.chart('expense-report', {
        chart: {
            type: 'pie'
        },
        title: {
            text: 'Expense grouped by category'
        },
        subtitle: {
            text: null
        },
        plotOptions: {
            series: {
                dataLabels: {
                    enabled: false,
                    format: '{point.name}: {point.y}'
                },
                showInLegend: true
            }
        },
        credits: {
            enabled: false
        },
        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}</b><br/>'
        },
        series: [{
            name: 'Category',
            colorByPoint: true,
            data: data
        }]
    });
}