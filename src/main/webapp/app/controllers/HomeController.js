(function() {
	function homeController($scope, $location, homeService, reminderService, localStorageService) {
		$scope.message = "I am in home";
		
		$scope.gotoExpenses = function() {
			$location.path("listExpenses");
		}		

		$scope.gotoRuns = function() {
			$location.path("listRuns");
		}

        reminderService.getDueBillReminders(function(data) {
            $scope.reminderList = data;
            for (var index = 0; index < $scope.reminderList.length; index++) {
                var date = new Date($scope.reminderList[index].nextReminder);
                $scope.reminderList[index].dueOn = date;
            }
        });

        $scope.payBill = function(bill) {
            var billToBePaid = {
                bill: bill,
                paid: false
            };
            localStorageService.put('billToBePaid', billToBePaid);
            $location.path("/addExpense");
        }

		/*homeService.getDashboardData(function(data) {
			for (var key in data) {
				$scope[key] = data[key];
			}
		});*/
	}
	
	helium.controller("HomeController", homeController)
})();