var helium = angular.module("helium", ['ngRoute']);

helium.config(function($routeProvider) {
	$routeProvider
	.when("/", { redirectTo: "/home"})
	.when("/home", {controller: "HomeController", templateUrl: "app/partials/home.html"})
	.when("/showExpense", {controller: "ExpenseController", templateUrl: "app/partials/show-expense.html"})
	.when("/addExpense", {controller: "ExpenseController", templateUrl: "app/partials/add-expense.html"})
	.when("/editExpense", {controller: "ExpenseController", templateUrl: "app/partials/edit-expense.html"})
	.when("/funds/showPortfolio", {controller: "FundController", templateUrl: "app/partials/show-portfolio.html"})
	.when("/funds/view", {controller: "FundController", templateUrl: "app/partials/view-fund.html"})
	.when("/funds/list", {controller: "FundController", templateUrl: "app/partials/list-funds.html"})
	.when("/funds/addTransaction", {controller: "FundController", templateUrl: "app/partials/add-fund-transaction.html"})
	.when("/overdraft/getDetails", {controller: "OverdraftController", templateUrl: "app/partials/get-overdraft-details.html"})
	.when("/overdraft/transact", {controller: "OverdraftController", templateUrl: "app/partials/transact-overdraft.html"})
	.when("/overdraft/changeLimit", {controller: "OverdraftController", templateUrl: "app/partials/change-limit-overdraft.html"})
	.when("/habits/monthLog", {controller: "HabitController", templateUrl: "app/partials/habit-calendar.html"})
	.when("/habits/add", {controller: "HabitController", templateUrl: "app/partials/add-new-habit.html"})
	.when("/listRuns", {controller: "RunController", templateUrl: "app/partials/list-runs.html"})
	.when("/addRun", {controller: "RunController", templateUrl: "app/partials/add-run.html"})
	.when("/editRun", {controller: "RunController", templateUrl: "app/partials/edit-run.html"})
});

helium.filter("sum", function() {
	return function(items, field) {
		var sum = 0;
		if (items != null && angular.isArray(items) && items.length > 0) {
			for (var index = 0; index < items.length; index++) {
				sum += items[index][field];
			}
		}
		return sum;
	}
})

helium.filter("max", function() {
	return function(items, field) {
		var max = 0;
		if (items != null && angular.isArray(items) && items.length > 0) {
			for (var index = 0; index < items.length; index++) {
				if (max < items[index][field]) {
					max = items[index][field];
				} 
			}
		}
		return max;
	}
})