package org.openshift.radium.dialogflow;

/**
 * Created by sankaranarayanan on 7/11/18.
 */
public class FulfillmentResponse {
    private String fulfillmentText;

    private Object[] fulfillmentMessages;

    private String source;

    private Object payload;

    private Object[] outputContexts;

    private Object followupEventInput;

    public String getFulfillmentText() {
        return fulfillmentText;
    }

    public void setFulfillmentText(String fulfillmentText) {
        this.fulfillmentText = fulfillmentText;
    }

    public Object[] getFulfillmentMessages() {
        return fulfillmentMessages;
    }

    public void setFulfillmentMessages(Object[] fulfillmentMessages) {
        this.fulfillmentMessages = fulfillmentMessages;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public Object getPayload() {
        return payload;
    }

    public void setPayload(Object payload) {
        this.payload = payload;
    }

    public Object[] getOutputContexts() {
        return outputContexts;
    }

    public void setOutputContexts(Object[] outputContexts) {
        this.outputContexts = outputContexts;
    }

    public Object getFollowupEventInput() {
        return followupEventInput;
    }

    public void setFollowupEventInput(Object followupEventInput) {
        this.followupEventInput = followupEventInput;
    }
}
