package org.openshift.radium.dialogflow;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by sankaranarayanan on 7/11/18.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class FulFillmentRequest {
    private String responseId;

    private QueryResult queryResult;

    public String getResponseId() {
        return responseId;
    }

    public void setResponseId(String responseId) {
        this.responseId = responseId;
    }

    public QueryResult getQueryResult() {
        return queryResult;
    }

    public void setQueryResult(QueryResult queryResult) {
        this.queryResult = queryResult;
    }
}
