package org.openshift.radium.dialogflow;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Map;

/**
 * Created by sankaranarayanan on 7/11/18.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class OutputContext {
    private String name;

    private Map<String, Object> parameters;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, Object> getParameters() {
        return parameters;
    }

    public void setParameters(Map<String, Object> parameters) {
        this.parameters = parameters;
    }
}
