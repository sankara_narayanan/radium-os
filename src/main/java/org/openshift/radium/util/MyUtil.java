package org.openshift.radium.util;

import org.openshift.radium.enums.BillReminderTaskEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

/**
 * Created by sankaranarayanan on 3/16/17.
 */
public class MyUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(MyUtil.class);

    private static Map<String, BillReminderTaskEnum> billReminderTaskMap = new HashMap<String, BillReminderTaskEnum>();

    public static Date getCurrentDate() throws Exception {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date currentDate = new Date();
        currentDate = dateFormat.parse(dateFormat.format(currentDate));
        LOGGER.info("Current date is: {}", currentDate);
        return currentDate;
    }

    public static Date toDate(String strDate) {
        Date date = null;
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
            date = dateFormat.parse(strDate);
            LOGGER.info("Converted date is: {}", date);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return date;
    }

    public static String toString(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy");
        return dateFormat.format(date);
    }

    public static void put(String key, BillReminderTaskEnum value) {
        billReminderTaskMap.put(key, value);
    }

    public static BillReminderTaskEnum get(String key) {
        return billReminderTaskMap.get(key);
    }

    public static void remove(String key) {
        billReminderTaskMap.remove(key);
    }

    public static Map<String, BillReminderTaskEnum> getBillReminderTask () {
        return billReminderTaskMap;
    }
}
