package org.openshift.radium.mail;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.mail.internet.MimeMessage;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

/**
 * Created by sankaranarayanan on 3/12/17.
 */
@Component
public class StockMailSender {
    @Autowired
    private JavaMailSenderImpl mailSender;

    @Value("${spring.mail.username}")
    private String sender;

    @PostConstruct
    public void setUpMailSender() {
        Properties javaMailProperties = new Properties();
        javaMailProperties.setProperty("mail.smtp.auth", "true");
        javaMailProperties.setProperty("mail.smtp.starttls.enable", "true");
        mailSender.setJavaMailProperties(javaMailProperties);
    }

    public void sendNotificationEmail(String notificationMail) throws Exception {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
        String today = dateFormat.format(new Date());

        MimeMessage message = mailSender.createMimeMessage();

        MimeMessageHelper helper = new MimeMessageHelper(message, true);

        helper.setFrom(sender);
        helper.setTo(sender);
        helper.setSubject(String.format("Stock notification for %s", today));
        helper.setText(notificationMail, true);

        mailSender.send(message);
    }
}
