package org.openshift.radium.mail;

import org.openshift.radium.model.mail.StockNotification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

/**
 * Created by sankaranarayanan on 3/13/17.
 */
@Component
public class StockMailCreator {
    @Autowired
    private TemplateEngine mailTemplateEngine;

    public String createNotificationMail(StockNotification stockNotification) {
        final Context mailContext = new Context();
        mailContext.setVariable("notification", stockNotification);

        final String notificationMail = this.mailTemplateEngine.process("Stock Notification", mailContext);
        return notificationMail;
    }
}
