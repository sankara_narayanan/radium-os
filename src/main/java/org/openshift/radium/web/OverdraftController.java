package org.openshift.radium.web;

import org.openshift.radium.dto.OverdraftTransactionDto;
import org.openshift.radium.model.OverdraftAccount;
import org.openshift.radium.service.api.IOverdraftService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by sankaranarayanan on 7/20/17.
 */
@RestController
@RequestMapping("overdraft")
public class OverdraftController {
    private static final Logger LOGGER = LoggerFactory.getLogger(OverdraftController.class);

    @Autowired
    private IOverdraftService odService;

    @RequestMapping(value = "transact", method = RequestMethod.POST)
    public void transact(@RequestBody OverdraftTransactionDto odTxnDto) {
        this.odService.transact(odTxnDto);
    }

    @RequestMapping(value = "changeLimit", method = RequestMethod.POST)
    public void changeLimit(@RequestBody OverdraftTransactionDto odTxnDto) {
        this.odService.changeLimit(odTxnDto);
    }

    @RequestMapping(value = "monthEndCalc", method = RequestMethod.GET)
    public void monthEndCalculation(@RequestParam(value = "monthEnd") Long monthEnd) throws Exception {
        this.odService.debtMonthEndInterest(monthEnd);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value = "getDetails", method = RequestMethod.GET)
    public OverdraftAccount getDetails() throws Exception {
        return this.odService.getDetails();
    }
}
