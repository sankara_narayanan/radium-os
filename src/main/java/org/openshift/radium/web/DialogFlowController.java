package org.openshift.radium.web;

import org.openshift.radium.dialogflow.FulFillmentRequest;
import org.openshift.radium.dialogflow.FulfillmentResponse;
import org.openshift.radium.dialogflow.QueryResult;
import org.openshift.radium.service.api.IDialogFlowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by sankaranarayanan on 7/11/18.
 */
@RestController
@RequestMapping("dialogFlow")
public class DialogFlowController {

    @Autowired
    private IDialogFlowService dialogFlowService;

    @RequestMapping(value = "handleDialogFlow", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public FulfillmentResponse handle(@RequestBody FulFillmentRequest request) {
        QueryResult queryResult = request.getQueryResult();
        System.out.println(String.format("Action is: %s", queryResult.getAction()));
        return dialogFlowService.handle(request);
    }
}
