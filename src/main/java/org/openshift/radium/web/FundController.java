package org.openshift.radium.web;

import org.openshift.radium.client.fund.FundClient;
import org.openshift.radium.dto.Fund;
import org.openshift.radium.dto.FundView;
import org.openshift.radium.model.FundTransaction;
import org.openshift.radium.model.PortfolioValue;
import org.openshift.radium.service.api.IFundService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * Created by sankaranarayanan on 4/8/17.
 */
@RestController
@RequestMapping(value = "funds")
public class FundController {
    private static final Logger LOGGER = LoggerFactory.getLogger(FundController.class);

    @Autowired
    private IFundService fundService;

    @Autowired
    private FundClient fundClient;

    @RequestMapping(value = "nav", method = RequestMethod.GET)
    public Double getNav(@RequestParam("fund") Long fund, @RequestParam("date") Long date) {
        LOGGER.info("Inside getNav(fund, date)");
        return this.fundService.getNav(fund, date);
    }

    @RequestMapping(value = "list", method = RequestMethod.GET)
    public Map<String, List<FundTransaction>> listTransactions() {
        LOGGER.info("Inside listTransactions()");
        return this.fundService.listTransactions();
    }

    @RequestMapping(value = "save", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public FundTransaction saveTransaction(@RequestBody FundTransaction fundTransaction) {
        LOGGER.info("Inside saveTransaction(FundTxn)");
        FundTransaction transaction = this.fundService.saveOrUpdateTransaction(fundTransaction);
        return transaction;
    }

    @RequestMapping(value = "refreshNav", method = RequestMethod.POST)
    public boolean refreshNav() throws Exception {
        LOGGER.info("Inside refreshNav()");
        this.fundClient.saveDailyNav();
        return true;
    }

    @RequestMapping(value = "getAllFundsValue", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public PortfolioValue getAllFundsValue() {
        return this.fundService.getPortfolioValue();
    }

    @RequestMapping(value = "view/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @CrossOrigin(origins = "http://localhost:3000")
    public Fund viewFund(@PathVariable("id") Long id) {
        return this.fundService.viewFund(id);
    }

    @RequestMapping(value = "listFunds", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<FundView> listFunds() {
        List<FundView> fundViewList = this.fundService.listFunds();
        return fundViewList;
    }
}
