package org.openshift.radium.web;

import org.openshift.radium.dto.ExpenseByItem;
import org.openshift.radium.model.DailyExpense;
import org.openshift.radium.service.api.IDailyExpenseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value="expense")
public class DailyExpenseController {
	private static final Logger LOGGER = LoggerFactory.getLogger(DailyExpenseController.class);
	
	@Autowired
	private IDailyExpenseService dailyExpenseService;
	
	@RequestMapping(value="save", method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
	public DailyExpense save(@RequestBody DailyExpense dailyExpense) {
		LOGGER.info("Inside save(DailyExpense)");
		return this.dailyExpenseService.save(dailyExpense);
	}
	
	@RequestMapping(value="update", method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
	public DailyExpense update(@RequestBody DailyExpense dailyExpense) {
		LOGGER.info("Inside update(DailyExpense)");
		return this.dailyExpenseService.update(dailyExpense);
	}
	
	@RequestMapping(value="list", method=RequestMethod.GET)
	public List<DailyExpense> list(@RequestParam("startTime") Long startTime, @RequestParam("endTime") Long endTime) {
		LOGGER.info("Inside list(Timestamp)");
		return this.dailyExpenseService.list(startTime, endTime);
	}
	
	@RequestMapping(value="delete", method=RequestMethod.POST)
	public Boolean delete(@RequestBody DailyExpense dailyExpense) {
		LOGGER.info("Inside delete(DailyExpense)");
		return this.dailyExpenseService.delete(dailyExpense);
	}
	
	@RequestMapping(value="getMonthlyReportByItem", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public List<ExpenseByItem> getMonthlyReportByItem(@RequestParam("startTime") Long startTime, @RequestParam("endTime") Long endTime) {
		LOGGER.info("Inside getMonthlyReportByItem(");
		return this.dailyExpenseService.getMonthlyReportByItem(startTime, endTime);
	}

	@RequestMapping(value="getCategoryItemMapping", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public Map<String, List<String>> getCategoryItemMapping() {
		LOGGER.info("Inside getCategoryItemMapping(");
		return this.dailyExpenseService.getCategoryItemMapping();
	}

	@RequestMapping(value = "getMonthlyExpenseList", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Map<Long, Double> getMonthlyExpenseList(@RequestParam("date") Long date) {
		return this.dailyExpenseService.getMonthlyExpenseList(date);
	}
}
