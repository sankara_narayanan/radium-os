package org.openshift.radium.web;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import org.openshift.radium.model.Run;
import org.openshift.radium.service.api.IRunService;

@RestController
@RequestMapping(value="run")
public class RunController {
	private static final Logger LOGGER  = LoggerFactory.getLogger(RunController.class);
	
	@Autowired
	private IRunService runService;
	
	@RequestMapping(value="list", method=RequestMethod.GET)
	public List<Run> listRuns(@RequestParam("startTime") Long startTime, @RequestParam("endTime") Long endTime) {
		LOGGER.info("Inside listRuns(Long,Long)");
		return this.runService.listRuns(startTime, endTime);
	}
	
	@RequestMapping(value="save", method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
	public Run saveRun(@RequestBody Run run) {
		LOGGER.info("Inside saveRun(Run)");
		return this.runService.saveRun(run);
	}
	
	@RequestMapping(value="update", method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
	public Run updateRun(@RequestBody Run run) {
		LOGGER.info("Inside updateRun(Run)");
		return this.runService.updateRun(run);
	}
	
	@RequestMapping(value="delete", method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE, consumes=MediaType.APPLICATION_JSON_VALUE)
	public Boolean deleteRun(@RequestBody Run run) {
		LOGGER.info("Inside deleteRun(Run)");
		return this.runService.deleteRun(run);
	}
}
