package org.openshift.radium.web;

import org.openshift.radium.model.BillReminder;
import org.openshift.radium.service.api.IReminderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by sankaranarayanan on 6/4/17.
 */
@RestController
@RequestMapping("reminders")
public class ReminderController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ReminderController.class);

    @Autowired
    private IReminderService reminderService;

    @RequestMapping(value = "dueBillReminders", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<BillReminder> getDueBillReminders() {
        LOGGER.info("Inside getDueBillReminders()");
        return this.reminderService.getDueBillReminders();
    }

    @RequestMapping(value = "updateNextReminder", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public Boolean updateNextReminder(@RequestBody BillReminder billReminder) {
        LOGGER.info("Inside updateNextReminder(BillReminder)");
        return this.reminderService.updateNextReminder(billReminder);
    }
}