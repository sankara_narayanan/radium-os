package org.openshift.radium.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import org.openshift.radium.dto.HomeDashboard;
import org.openshift.radium.service.api.IHomeService;

@RestController
public class HomeController {
	private static final Logger LOGGER = LoggerFactory.getLogger(HomeController.class);
	
	@Autowired
	private IHomeService homeService;
	
	@RequestMapping(value="getDashboardData", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public HomeDashboard getDashboardData() {
		LOGGER.info("Inside getDashboardData()");
		return this.homeService.getDashboardData(); 
	}
}
