package org.openshift.radium.web;

import org.openshift.radium.dto.HabitMonthLog;
import org.openshift.radium.model.Habit;
import org.openshift.radium.service.api.IHabitLogService;
import org.openshift.radium.service.api.IHabitService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by sankaranarayanan on 6/30/18.
 */
@RestController
@RequestMapping("habits")
public class HabitController {
    private static final Logger logger = LoggerFactory.getLogger(HabitController.class);

    @Autowired
    private IHabitService habitService;

    @Autowired
    private IHabitLogService habitLogService;

    @RequestMapping(value = "list", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Habit> getAllHabits() {
        return habitService.getAllHabits();
    }

    @RequestMapping(value = "save", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public void save(@RequestBody Habit habit) {
        habitService.save(habit);
    }

    //1988
    @RequestMapping(value = "monthLog", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public HabitMonthLog getMonthLog(@RequestParam("habitId") Long habitId, @RequestParam("date") Long date) {
        return this.habitLogService.getMonthLog(habitId, date);
    }

    @RequestMapping(value = "log", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public void logHabit(@RequestParam("habitId") Long habitId, @RequestParam("date") Long date, @RequestParam("isCompleted") Boolean isCompleted) {
        this.habitLogService.logHabit(habitId, date, isCompleted);
    }
}
