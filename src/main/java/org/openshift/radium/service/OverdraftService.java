package org.openshift.radium.service;

import org.openshift.radium.dto.OverdraftTransactionDto;
import org.openshift.radium.model.OverdraftAccount;
import org.openshift.radium.model.OverdraftTransaction;
import org.openshift.radium.repo.api.IOverdraftAccountRepository;
import org.openshift.radium.repo.api.IOverdraftTransactionRepository;
import org.openshift.radium.service.api.IOverdraftService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by sankaranarayanan on 7/20/17.
 */
@Service("OverdraftService")
public class OverdraftService implements IOverdraftService{
    private static final Logger LOGGER = LoggerFactory.getLogger(OverdraftService.class);

    private static final Long ONE_DAY_IN_MILLIS = 24 * 60 * 60 * 1000L;

    @Autowired
    private IOverdraftAccountRepository odAccRepo;

    @Autowired
    private IOverdraftTransactionRepository odTxnRepo;

    @Override
    public OverdraftAccount getDetails() throws ParseException {
        LOGGER.info("Inside getDetails()");

        OverdraftAccount odAcc = odAccRepo.findOne(1L);
        Double interest = calculateInterest(new Date().getTime());
        odAcc.setInterest(interest);

        return odAcc;
    }

    @Override
    public void transact(OverdraftTransactionDto odTxnDto) {
        LOGGER.info("Inside transact(odTxnDto)");

        OverdraftAccount odAcc = odAccRepo.findOne(1L);

        // Update Book balance & Available balance
        if (odTxnDto.getComment().equalsIgnoreCase("Principal") || odTxnDto.getComment().equalsIgnoreCase("Loan Principal") ) {
            Double drawingPower = odAcc.getDrawingPower() + odTxnDto.getAmount();
            odAcc.setDrawingPower(drawingPower);
        }
        else {
            Double bookBalance = odAcc.getBookBalance() + odTxnDto.getAmount();
            Double availableBalance = odAcc.getAvailableBalance() + odTxnDto.getAmount();
            odAcc.setBookBalance(bookBalance);
            odAcc.setAvailableBalance(availableBalance);
        }

        // Insert a transaction record
        OverdraftTransaction odTxn = new OverdraftTransaction();
        odTxn.setDate(odTxnDto.getDate());
        odTxn.setComment(odTxnDto.getComment());
        odTxn.setBookBalance(odAcc.getBookBalance());
        odTxn.setAvailableBalance(odAcc.getAvailableBalance());
        odTxn.setLimit(odAcc.getLimit());
        odTxn.setDrawingPower(odAcc.getDrawingPower());
        if (odTxnDto.getAmount() > 0) {
            odTxn.setCredit(odTxnDto.getAmount());
            odTxn.setDebit(0D);
        }
        else {
            odTxn.setCredit(0D);
            odTxn.setDebit(odTxnDto.getAmount());
        }

        odTxnRepo.save(odTxn);
        odAccRepo.save(odAcc);
    }

    @Override
    public void changeLimit(OverdraftTransactionDto odTxnDto) {
        LOGGER.info("Inside changeLimit(odTxnDto)");

        OverdraftAccount odAcc = odAccRepo.findOne(1L);

        // Update Book balance & Available balance
        Double differenceInLimit = odAcc.getLimit() - odTxnDto.getAmount();
        Double availableBalance = odAcc.getAvailableBalance() - differenceInLimit;
        odAcc.setLimit(odTxnDto.getAmount());
        odAcc.setDrawingPower(odTxnDto.getAmount());
        odAcc.setAvailableBalance(availableBalance);

        // Insert a transaction record
        OverdraftTransaction odTxn = new OverdraftTransaction();
        odTxn.setDate(odTxnDto.getDate());
        odTxn.setComment("Change in limit");
        odTxn.setBookBalance(odAcc.getBookBalance());
        odTxn.setAvailableBalance(availableBalance);
        odTxn.setLimit(odAcc.getLimit());
        odTxn.setDrawingPower(odAcc.getDrawingPower());
        odTxn.setCredit(0D);
        odTxn.setDebit(0D);
        //odTxn.setInterest(interest);

        odTxnRepo.save(odTxn);
        odAccRepo.save(odAcc);
    }

    @Override
    public void debtMonthEndInterest(Long monthEnd) throws Exception {
        LOGGER.info("Inside calculateMonthlyStatement()");
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

        // End date
        Date endDate = formatter.parse(formatter.format(new Date(monthEnd)));

        // Calculate interest
        Double interest = this.calculateInterest(monthEnd);

        // Debit interest
        OverdraftTransactionDto odTxnDto = new OverdraftTransactionDto();
        odTxnDto.setDate(endDate.getTime());
        odTxnDto.setAmount(interest);
        odTxnDto.setComment("Month end interest");
        this.transact(odTxnDto);
    }

    private Double calculateInterest(Long monthEnd) throws ParseException {
        Double interest = 0D;
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

        // End date
        Date endDate = new Date(monthEnd);

        // Start date
        Calendar startCalendar = Calendar.getInstance();
        startCalendar.setTime(formatter.parse(formatter.format(new Date(monthEnd))));
        startCalendar.set(Calendar.DATE, 1);
        Date startDate = new Date(startCalendar.getTimeInMillis());

        // Calculate intermediate interest
        Long lastTxnDate = null;
        OverdraftTransaction lastTxn = null;
        int noOfDays = Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH);

        List<OverdraftTransaction> odTxnList = odTxnRepo.findByDateBetweenOrderByDateAsc(startDate.getTime() - ONE_DAY_IN_MILLIS, endDate.getTime());
        for (OverdraftTransaction odTxn : odTxnList) {
            if (odTxnList.indexOf(odTxn) == 0) {
                lastTxn = odTxn;
                lastTxnDate = startDate.getTime();
                continue;
            }

            Long daysInMilliSec = odTxn.getDate() - lastTxnDate;
            Long days = daysInMilliSec / (1000 * 60 * 60 * 24);

            noOfDays -= days;
            interest += simpleInterest(lastTxn.getBookBalance(), days, 8.35);

            lastTxn = odTxn;
            lastTxnDate = lastTxn.getDate();
        }

        // Calculate month end interest
        interest += simpleInterest(lastTxn.getBookBalance(), Long.valueOf(noOfDays), 8.35);

        return interest;
    }

    private Long simpleInterest(Double principal, Long days, Double interest) {
        return Math.round(principal * days * interest / (365 * 100));
    }
}
