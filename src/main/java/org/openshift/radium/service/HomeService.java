package org.openshift.radium.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.openshift.radium.dto.ExpenseItem;
import org.openshift.radium.dto.HomeDashboard;
import org.openshift.radium.dto.RunItem;
import org.openshift.radium.repo.api.IDailyExpenseRepository;
import org.openshift.radium.repo.api.IRunRepository;
import org.openshift.radium.service.api.IHomeService;

@Service("homeService")
public class HomeService implements IHomeService {
	private static final Logger LOGGER = LoggerFactory.getLogger(HomeService.class);
	
	@Autowired
	private IDailyExpenseRepository dailyExpenseRepo;
	
	@Autowired
	private IRunRepository runRepo;
	
	@Override
	public HomeDashboard getDashboardData() {
		LOGGER.info("Inside getDashboardData()");
		HomeDashboard homeDashboard = new HomeDashboard();

		List<Object[]> sumOfExpenses = this.dailyExpenseRepo.findSumOfExpenseGroupByMonth();
		for (Object[] obj : sumOfExpenses) {
			ExpenseItem expense = new ExpenseItem();
			expense.setMonth((String) obj[1]);
			expense.setExpense((Double) obj[2]);
			homeDashboard.getExpenses().add(expense);
		}
		
		List<Object[]> sumOfDistances = this.runRepo.findSumOfDistanceGroupByMonth();
		for (Object[] obj : sumOfDistances) {
			RunItem run = new RunItem();
			run.setMonth((String) obj[1]);
			run.setDistance((Double) obj[2]);
			homeDashboard.getRuns().add(run);
		}

		return homeDashboard;
	}

}
