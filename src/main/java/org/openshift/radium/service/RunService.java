package org.openshift.radium.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.openshift.radium.model.Run;
import org.openshift.radium.repo.api.IRunRepository;
import org.openshift.radium.service.api.IRunService;

@Service("runService")
public class RunService implements IRunService {
	private static final Logger LOGGER = LoggerFactory.getLogger(RunService.class);
	
	@Autowired
	private IRunRepository runRepo;
	
	@Override
	public List<Run> listRuns(Long startTime, Long endTime) {
		LOGGER.info("Inside listRuns(Long, Long)");
		List<Run> runs = this.runRepo.findByRunDateBetweenOrderByRunDateDesc(startTime, endTime);
		return runs;
	}

	@Override
	public Run saveRun(Run run) {
		LOGGER.info("Inside saveRun(Run)");
		Run persistedRun = this.runRepo.save(run);
		return persistedRun; 
	}

	@Override
	public Run updateRun(Run run) {
		LOGGER.info("Inside updateRun(Run)");
		Run persistedRun = this.runRepo.save(run);
		return persistedRun; 
	}

	@Override
	public Boolean deleteRun(Run run) {
		LOGGER.info("Inside deleteRun(Run)");
		this.runRepo.delete(run);
		return true;
	}

}
