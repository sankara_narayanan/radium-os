package org.openshift.radium.service;

import org.openshift.radium.model.Habit;
import org.openshift.radium.repo.api.IHabitRepository;
import org.openshift.radium.service.api.IHabitService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by sankaranarayanan on 6/30/18.
 */
@Service("habitService")
public class HabitService implements IHabitService {
    private static final Logger logger = LoggerFactory.getLogger(HabitService.class);

    @Autowired
    private IHabitRepository habitRepo;

    @Override
    public List<Habit> getAllHabits() {
        return habitRepo.findAll();
    }

    @Override
    public void save(Habit habit) {
        this.habitRepo.save(habit);
    }
}
