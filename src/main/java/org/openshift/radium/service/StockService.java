package org.openshift.radium.service;

import org.openshift.radium.client.stock.google.GoogleFinanceClient;
import org.openshift.radium.enums.StockEnum;
import org.openshift.radium.mail.StockMailCreator;
import org.openshift.radium.mail.StockMailSender;
import org.openshift.radium.model.Stock;
import org.openshift.radium.model.mail.StockNotification;
import org.openshift.radium.service.api.IStockService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by sankaranarayanan on 3/10/17.
 */
@Service
public class StockService implements IStockService{
    private static final Logger LOGGER = LoggerFactory.getLogger(StockService.class);

    @Autowired
    private StockMailCreator stockMailCreator;

    @Autowired
    private StockMailSender stockMailSender;

    @Autowired
    private GoogleFinanceClient googleClient;

    @Override
    public List<Stock> getWatchlist() {
        return null;
    }

    @Override
    public void sendNotificationMail() throws Exception {
        StockNotification stockNotification = new StockNotification();
        stockNotification.setWatchList(new ArrayList<Stock>());

        // Add stock price to watchlist
        for (StockEnum stockEnum : StockEnum.values()) {
            Stock stock = this.googleClient.getStock("NSE", stockEnum.name());
            stockNotification.getWatchList().add(stock);
        }

        if (stockNotification.getWatchList().isEmpty()) {
            LOGGER.info("Market is closed today. There are no notifications to be sent.");
            return;
        }

        Collections.sort(stockNotification.getWatchList(), Collections.<Stock>reverseOrder());

        // Send watch list notification email
        String notificationMail = this.stockMailCreator.createNotificationMail(stockNotification);
        this.stockMailSender.sendNotificationEmail(notificationMail);
    }
}
