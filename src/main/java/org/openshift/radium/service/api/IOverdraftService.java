package org.openshift.radium.service.api;

import org.openshift.radium.dto.OverdraftTransactionDto;
import org.openshift.radium.model.OverdraftAccount;

import java.text.ParseException;

/**
 * Created by sankaranarayanan on 7/20/17.
 */
public interface IOverdraftService {
    OverdraftAccount getDetails() throws ParseException;

    void transact(OverdraftTransactionDto odTxnDto);

    void changeLimit(OverdraftTransactionDto odTxnDto);

    void debtMonthEndInterest(Long monthEnd) throws Exception;
}
