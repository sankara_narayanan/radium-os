package org.openshift.radium.service.api;

import org.openshift.radium.dto.ExpenseByItem;
import org.openshift.radium.model.DailyExpense;

import java.util.List;
import java.util.Map;

public interface IDailyExpenseService {
	DailyExpense save(DailyExpense dailyExpense);

	DailyExpense save(String item, Double cost);

	DailyExpense update(DailyExpense dailyExpense);
	
	List<DailyExpense> list(Long startTime, Long endTime);
	
	Boolean delete(DailyExpense dailyExpense);
	
	List<ExpenseByItem> getMonthlyReportByItem(Long startTime, Long endTime);

	Map<String, List<String>> getCategoryItemMapping();

	Map<Long, Double> getMonthlyExpenseList(Long date);
}
