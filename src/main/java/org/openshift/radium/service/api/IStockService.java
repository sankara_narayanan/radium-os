package org.openshift.radium.service.api;

import org.openshift.radium.model.Stock;

import java.util.List;

/**
 * Created by sankaranarayanan on 3/10/17.
 */
public interface IStockService {
    List<Stock> getWatchlist();

    void sendNotificationMail() throws Exception;
}
