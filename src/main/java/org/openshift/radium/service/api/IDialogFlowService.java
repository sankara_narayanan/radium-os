package org.openshift.radium.service.api;

import org.openshift.radium.dialogflow.FulFillmentRequest;
import org.openshift.radium.dialogflow.FulfillmentResponse;

/**
 * Created by sankaranarayanan on 7/11/18.
 */
public interface IDialogFlowService {
    FulfillmentResponse handle(FulFillmentRequest request);
}
