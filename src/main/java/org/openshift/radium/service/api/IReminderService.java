package org.openshift.radium.service.api;

import org.openshift.radium.model.BillReminder;

import java.util.List;

/**
 * Created by sankaranarayanan on 6/4/17.
 */
public interface IReminderService {
    List<BillReminder> getDueBillReminders();

    Boolean updateNextReminder(BillReminder reminder);
}
