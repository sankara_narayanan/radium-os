package org.openshift.radium.service.api;

import org.openshift.radium.dto.HabitMonthLog;

/**
 * Created by sankaranarayanan on 4/7/17.
 */
public interface IHabitLogService {
    void logHabit(Long habitId, Long date, Boolean isCompleted);

    HabitMonthLog getMonthLog(Long habitId, Long date);
}
