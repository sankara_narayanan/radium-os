package org.openshift.radium.service.api;

import org.openshift.radium.dto.HomeDashboard;

public interface IHomeService {
	HomeDashboard getDashboardData();
}
