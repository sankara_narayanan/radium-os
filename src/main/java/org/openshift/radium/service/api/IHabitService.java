package org.openshift.radium.service.api;

import org.openshift.radium.model.Habit;

import java.util.List;

/**
 * Created by sankaranarayanan on 4/7/17.
 */
public interface IHabitService {
    List<Habit> getAllHabits();

    void save(Habit habit);
}
