package org.openshift.radium.service.api;

import java.util.List;

import org.openshift.radium.model.Run;

public interface IRunService {
	List<Run> listRuns(Long startTime, Long endTime);
	
	Run saveRun(Run run);
	
	Run updateRun(Run run);

	Boolean deleteRun(Run run);
}
