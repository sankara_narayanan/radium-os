package org.openshift.radium.service.api;

import org.openshift.radium.dto.Fund;
import org.openshift.radium.dto.FundValue;
import org.openshift.radium.dto.FundView;
import org.openshift.radium.model.FundNav;
import org.openshift.radium.model.FundTransaction;
import org.openshift.radium.model.PortfolioValue;

import java.util.List;
import java.util.Map;

/**
 * Created by sankaranarayanan on 4/7/17.
 */
public interface IFundService {
    Double getNav(Long fund, Long date);

    FundNav saveDailyNav(FundNav fundNav);

    Boolean saveDailyPortfolioValue();

    Map<String, List<FundTransaction>> listTransactions();

    FundTransaction saveOrUpdateTransaction(FundTransaction fundTransaction);

    PortfolioValue getPortfolioValue();

    Fund viewFund(Long id);

    List<FundValue> getAllFundsValue();

    List<FundView> listFunds();
}
