package org.openshift.radium.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.openshift.radium.dialogflow.FulFillmentRequest;
import org.openshift.radium.dialogflow.FulfillmentResponse;
import org.openshift.radium.dialogflow.QueryResult;
import org.openshift.radium.model.DailyExpense;
import org.openshift.radium.service.api.IDailyExpenseService;
import org.openshift.radium.service.api.IDialogFlowService;
import org.openshift.radium.util.MyUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by sankaranarayanan on 7/11/18.
 */
@Service("dialogFlowService")
public class DialogFlowService implements IDialogFlowService {
    private static final Logger logger = LoggerFactory.getLogger(DialogFlowService.class);

    @Autowired
    private IDailyExpenseService dailyExpenseService;

    @Override
    public FulfillmentResponse handle(FulFillmentRequest request) {
        FulfillmentResponse response = new FulfillmentResponse();
        QueryResult queryResult = request.getQueryResult();

        String action = queryResult.getAction();
        System.out.println(String.format("Action is: %s", action));

        Map<String, Object> parameters = queryResult.getParameters();
        if ("saveExpense.cost".equalsIgnoreCase(action)) {
            String item = String.valueOf(parameters.get("item"));
            Double cost = Double.valueOf(parameters.get("cost").toString());
            dailyExpenseService.save(item, cost);
            response.setFulfillmentText(queryResult.getFulfillmentText());
        } else if ("showExpense".equalsIgnoreCase(action)) {
            Map<String, Object> payload = this.handleListExpense(parameters);
            //response.setFulfillmentText(queryResult.getFulfillmentText());
            //response.setFulfillmentMessages(new Object[]{payload});
            response.setPayload(payload);
        }

        return response;
    }

    private Map<String, Object> handleListExpense(Map<String, Object> parameters){
        Map<String, Object> payload = new HashMap<String, Object>();
        try {
            String timestamp = parameters.get("date").toString();
            String strDate = timestamp.split("T")[0];
            Date date = MyUtil.toDate(strDate);

            Long startTime = date.getTime();
            Long endTime = date.getTime() + (24 * 60 * 60 * 1000);
            List<DailyExpense> expenseList = dailyExpenseService.list(startTime, endTime);

            ObjectMapper objectMapper = new ObjectMapper();
            if (expenseList.size() == 0) {
                InputStream listExpensesStream = this.getClass().getClassLoader().getResourceAsStream("list-expenses-0.json");
                payload = objectMapper.readValue(listExpensesStream, Map.class);
                return payload;
            }

            InputStream listExpensesStream = this.getClass().getClassLoader().getResourceAsStream("list-expenses-1.json");
            JsonNode jsonNode = objectMapper.readTree(listExpensesStream);
            ArrayNode itemsNode = (ArrayNode) jsonNode.findPath("google")
                    .findPath("richResponse")
                    .findPath("items");

            /* Creation of rows json node*/
            Double total = 0D;
            ArrayNode rows = objectMapper.createArrayNode();
            for (DailyExpense dailyExpense : expenseList) {
                total += dailyExpense.getCost();

                ArrayNode cells = objectMapper.createArrayNode();
                cells.add(objectMapper.createObjectNode().put("text", dailyExpense.getCategory()));
                cells.add(objectMapper.createObjectNode().put("text", dailyExpense.getItem()));
                cells.add(objectMapper.createObjectNode().put("text", dailyExpense.getCost().toString()));

                ObjectNode dummyNode = objectMapper.createObjectNode();
                dummyNode.set("cells", cells);
                rows.add(dummyNode);
            }

            String simpleReponse = String.format("Total expense on %s is %s. And here are the expenses", MyUtil.toString(date), total);
            ObjectNode simpleResponse = (ObjectNode) itemsNode.get(0).findPath("simpleResponse");
            simpleResponse.put("textToSpeech", simpleReponse);

            ObjectNode tableCard = (ObjectNode) itemsNode.get(1).findPath("tableCard");
            tableCard.put("subtitle", "on " + MyUtil.toString(date));
            tableCard.set("rows", rows);

            payload = objectMapper.convertValue(jsonNode, Map.class);
        } catch(Exception ex) {
            ex.printStackTrace();
        }
        return payload;
    }
}
