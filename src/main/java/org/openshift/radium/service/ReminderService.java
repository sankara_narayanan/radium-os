package org.openshift.radium.service;

import org.openshift.radium.model.BillReminder;
import org.openshift.radium.repo.api.IBillReminderRepository;
import org.openshift.radium.service.api.IReminderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by sankaranarayanan on 6/4/17.
 */
@Service("reminderService")
public class ReminderService implements IReminderService{
    private static final Logger LOGGER = LoggerFactory.getLogger(ReminderService.class);

    @Autowired
    private IBillReminderRepository billReminderRepo;

    @Override
    public List<BillReminder> getDueBillReminders() {
        LOGGER.info("Inside getDueBillReminder()");

        Long oneDayInMillis = new Long(2 * 24 * 60 * 60 * 1000);
        Date todaysDate = new Date();
        List<BillReminder> dueBillReminderList = new ArrayList<BillReminder>();

        List<BillReminder> billReminderList = this.billReminderRepo.findByNextReminderNot(0L);
        for(BillReminder reminder : billReminderList) {
            Long dueTimeLeft = reminder.getNextReminder() - todaysDate.getTime();

            if (dueTimeLeft < oneDayInMillis) {
                dueBillReminderList.add(reminder);
            }
        }

        return dueBillReminderList;
    }

    @Override
    public Boolean updateNextReminder(BillReminder paramBillReminder) {
        LOGGER.info("Inside updateNextReminder(BillReminder)");

        BillReminder billReminder = this.billReminderRepo.findById(paramBillReminder.getId());

        Calendar nextReminder = Calendar.getInstance();
        nextReminder.setTime(new Date(billReminder.getNextReminder()));
        nextReminder.add(Calendar.MONTH, 1);

        // Update existing reminder due date 0
        billReminder.setNextReminder(0L);
        this.billReminderRepo.save(billReminder);

        BillReminder nextBillReminder = new BillReminder();
        BeanUtils.copyProperties(billReminder, nextBillReminder);
        nextBillReminder.setNextReminder(nextReminder.getTimeInMillis());
        nextBillReminder.setId(null);
        this.billReminderRepo.save(nextBillReminder);

        return true;
    }

    private String formatDate(Long dueDateMillis) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMMMM dd, yyyy");
        Date dueDate = new Date(dueDateMillis);
        String dueDateStr = dateFormat.format(dueDate);
        return dueDateStr;
    }
}
