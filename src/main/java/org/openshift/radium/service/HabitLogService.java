package org.openshift.radium.service;

import org.openshift.radium.dto.HabitMonthLog;
import org.openshift.radium.model.Habit;
import org.openshift.radium.model.HabitLog;
import org.openshift.radium.repo.api.IHabitLogRepository;
import org.openshift.radium.repo.api.IHabitRepository;
import org.openshift.radium.service.api.IHabitLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thymeleaf.util.ArrayUtils;

import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by sankaranarayanan on 6/30/18.
 */
@Service("habitLogService")
public class HabitLogService implements IHabitLogService{
    @Autowired
    private IHabitRepository habitRepo;

    @Autowired
    private IHabitLogRepository habitLogRepo;

    @Override
    public void logHabit(Long habitId, Long date, Boolean isCompleted) {
        Habit habit = this.habitRepo.findOne(habitId);
        //habit.setId(habitId);

        HabitLog habitLog = this.habitLogRepo.findByHabitAndDate(habit, date);
        if (habitLog == null)
                habitLog = new HabitLog();

        habitLog.setHabit(habit);
        habitLog.setDate(date);
        habitLog.setDone(isCompleted);

        this.habitLogRepo.save(habitLog);
    }

    @Override
    public HabitMonthLog getMonthLog(Long habitId, Long paramDate) {
        Habit habit = this.habitRepo.findOne(habitId);

        HabitMonthLog habitMonthLog = new HabitMonthLog();
        Calendar calendar = getCalendar(paramDate);

        calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
        Long startDate = calendar.getTimeInMillis() / 1000;

        calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        Long endDate = calendar.getTimeInMillis() / 1000;

        List<HabitLog> habitLogList = habitLogRepo.findByHabitAndDateBetween(habit, startDate, endDate);

        for (HabitLog habitLog : habitLogList) {
            Long lDate = habitLog.getDate();
            calendar.setTimeInMillis(lDate * 1000);
            if (habitLog.getDone()) {
                // Completed
                habitMonthLog.getDaysCompleted().add(calendar.get(Calendar.DATE));
            } else {
                // Uncompleted
                habitMonthLog.getDaysUncompleted().add(calendar.get(Calendar.DATE));
            }
        }

        String[] daysCommitted = habit.getDays().split(",");
        for (Long date = startDate; date <= endDate; date += 86400) {
            calendar.setTimeInMillis(date * 1000);
            String day = calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.US);
            if (ArrayUtils.contains(daysCommitted, day)) {
                // Days committed
                habitMonthLog.getDaysCommitted().add(calendar.get(Calendar.DATE));
            }
        }

        Calendar today = Calendar.getInstance(TimeZone.getTimeZone("IST"));
        Calendar habitCalendar = getCalendar(paramDate);
        for (Integer dayCommitted : habitMonthLog.getDaysCommitted()) {
            habitCalendar.set(Calendar.DATE, dayCommitted);
            if (habitCalendar.getTimeInMillis() <= today.getTimeInMillis()) {
                if (!(habitMonthLog.getDaysCompleted().contains(dayCommitted) ||
                        habitMonthLog.getDaysUncompleted().contains(dayCommitted))) {
                    // Days pending
                    habitMonthLog.getDaysPending().add(dayCommitted);
                }
            }
        }

        habitMonthLog.setHabitId(habitId);

        return habitMonthLog;
    }

    private Calendar getCalendar(Long date) {
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("IST"));
        calendar.setTimeInMillis(date);
        calendar.set(Calendar.AM_PM, Calendar.AM);
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar;
    }
}
