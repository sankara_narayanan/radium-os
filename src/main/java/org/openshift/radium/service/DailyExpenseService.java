package org.openshift.radium.service;

import org.openshift.radium.dto.ExpenseByItem;
import org.openshift.radium.model.CategoryItem;
import org.openshift.radium.model.DailyExpense;
import org.openshift.radium.repo.api.ICategoryItemRepository;
import org.openshift.radium.repo.api.IDailyExpenseRepository;
import org.openshift.radium.service.api.IDailyExpenseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service("dailyExpenseService")
public class DailyExpenseService implements IDailyExpenseService {
	private static final Logger LOGGER = LoggerFactory.getLogger(DailyExpenseService.class);
	
	@Autowired
	private IDailyExpenseRepository dailyExpenseRepo;

	@Autowired
	private ICategoryItemRepository categoryItemRepo;

	@Override
	public DailyExpense save(DailyExpense dailyExpense) {
		LOGGER.info("Inside save(DailyExpense)");
		DailyExpense persistedDailyExpense = this.dailyExpenseRepo.save(dailyExpense);
		return persistedDailyExpense;
	}

	@Override
	public DailyExpense update(DailyExpense dailyExpense) {
		LOGGER.info("Inside update(DailyExpense)");
		DailyExpense persistedDailyExpense = this.dailyExpenseRepo.save(dailyExpense);
		return persistedDailyExpense;
	}

	@Override
	public List<DailyExpense> list(Long expenseDate) {
		LOGGER.info("Inside list(Date)");
		return this.dailyExpenseRepo.findByExpenseDate(expenseDate);
	}

	@Override
	public Boolean delete(DailyExpense dailyExpense) {
		LOGGER.info("Inside delete(DailyExpense)");
		this.dailyExpenseRepo.delete(dailyExpense);
		return true;
	}

	@Override
	public List<ExpenseByItem> getMonthlyReportByItem(Long startTime, Long endTime) {
		LOGGER.info("Inside getMonthlyReportByItem()");
		
		List<ExpenseByItem> report = new ArrayList<ExpenseByItem>();
		List<Object[]> queryResults = this.dailyExpenseRepo.findSumOfExpenseGroupByItem(startTime, endTime);
		for (Object[] obj : queryResults) {
			ExpenseByItem expense = new ExpenseByItem();
			expense.setCategory((String) obj[0]);
			expense.setItem((String) obj[1]);
			expense.setExpense((Double) obj[2]);
			report.add(expense);
		}
		
		return report;
	}

	@Override
	public Map<String, List<String>> getCategoryItemMapping() {
		LOGGER.info("Inside getCategoryItemMapping()");

		Map<String, List<String>> categoryItemMap = new LinkedHashMap<String, List<String>>();
		List<CategoryItem> categoryItemList = this.categoryItemRepo.findAll();
		for (CategoryItem categoryItem : categoryItemList) {
			List<String> itemList = categoryItemMap.get(categoryItem.getCategory());

			if (itemList == null) {
				itemList = new ArrayList<String>();
				categoryItemMap.put(categoryItem.getCategory(), itemList);
			}

			itemList.add(categoryItem.getItem());
		}
		return categoryItemMap;
	}

	@Override
	public Map<Long, Double> getMonthlyExpenseList(Long paramDate) {
		LOGGER.info("Inside getMonthlyExpenseList(date)");

		Map<Long, Double> monthlyExpenseList = new LinkedHashMap<Long, Double>();
		Calendar calendar = getCalendar(paramDate);

		calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
		Long startDate = calendar.getTimeInMillis();

		calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		Long endDate = calendar.getTimeInMillis();

		List<Object[]> queryResult = this.dailyExpenseRepo.findSumOfCostGroupByExpenseDate(startDate, endDate);
		for (Object[] obj : queryResult) {
			Long date = (Long) obj[0];
			Double cost = (Double) obj[1];
			monthlyExpenseList.put(date, cost);
		}

		return monthlyExpenseList;
	}

	private Calendar getCalendar(Long date) {
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("IST"));
		calendar.setTimeInMillis(date);
		calendar.set(Calendar.AM_PM, Calendar.AM);
		calendar.set(Calendar.HOUR, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar;
	}
}
