package org.openshift.radium.service;

import org.openshift.radium.client.fund.FundClient;
import org.openshift.radium.dto.*;
import org.openshift.radium.enums.FundEnum;
import org.openshift.radium.model.DailyPortfolioValue;
import org.openshift.radium.model.FundNav;
import org.openshift.radium.model.FundTransaction;
import org.openshift.radium.model.PortfolioValue;
import org.openshift.radium.repo.api.IDailyPortfolioValueRepository;
import org.openshift.radium.repo.api.IFundNavRepository;
import org.openshift.radium.repo.api.IFundTransactionRepository;
import org.openshift.radium.service.api.IFundService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by sankaranarayanan on 4/7/17.
 */
@Service("fundService")
public class FundService implements IFundService {
    private static final Logger LOGGER = LoggerFactory.getLogger(FundService.class);

    @Autowired
    private FundClient fundClient;

    @Autowired
    private IFundNavRepository fundNavRepo;

    @Autowired
    private IFundTransactionRepository fundTxnrepo;

    @Autowired
    private IDailyPortfolioValueRepository dailyPortfolioValueRepo;

    @Override
    public Map<String, List<FundTransaction>> listTransactions() {
        LOGGER.info("Inside listFundTransactions()");
        Map<String, List<FundTransaction>> fundTxnListByName = new LinkedHashMap<String, List<FundTransaction>>();
        for (FundEnum fundEnum : FundEnum.values()) {
            List<FundTransaction> fundTxnList = this.fundTxnrepo.findTop6ByFundOrderByDateDesc(fundEnum.fundCode());
            fundTxnListByName.put(fundEnum.fundName(), fundTxnList);
        }
        return fundTxnListByName;
    }

    @Override
    public Double getNav(Long fund, Long date) {
        LOGGER.info("Inside getNav(fund, date)");
        FundEnum fundEnum = FundEnum.findByFundCode(fund);
        return this.fundClient.pullNav(fundEnum, date);
    }

    @Override
    public FundNav saveDailyNav(FundNav fundNav) {
        LOGGER.info("Inside saveDailyNav(FundNav)");
        FundNav persistedFundNav = this.fundNavRepo.save(fundNav);
        return persistedFundNav;
    }

    @Override
    public FundTransaction saveOrUpdateTransaction(FundTransaction fundTransaction) {
        LOGGER.info("Inside saveOrUpdateTransaction(FundTxn)");
        FundTransaction persistedTransaction = this.fundTxnrepo.save(fundTransaction);
        return persistedTransaction;
    }

    @Override
    public Boolean saveDailyPortfolioValue() {
        LOGGER.info("Inside saveDailyPortfolioValue()");

        // Get all funds' purchase value and actual value
        List<FundValue> fundValueList = this.getAllFundsValue();

        // Calculate total invested value and current value
        Double investedValue = 0d;
        Double currentValue = 0d;
        for (FundValue fundValue : fundValueList) {
            investedValue += fundValue.getPurchaseValue();
            currentValue += fundValue.getActualValue();
        }

        // Create DailyPortfolioValue object
        DailyPortfolioValue portfolioValue = new DailyPortfolioValue();
        portfolioValue.setDate(new Date().getTime());
        portfolioValue.setInvestedValue(investedValue);
        portfolioValue.setCurrentValue(currentValue);

        // Save DailyPortfolioValue
        this.dailyPortfolioValueRepo.save(portfolioValue);
        return true;
    }

    public PortfolioValue getPortfolioValue() {
        // Calculate purchase value of each fund
        List<FundValue> fundValueList = this.fundTxnrepo.getSumOfPurchaseValueGroupByFund();
        for (FundValue fundValue : fundValueList) {
            FundEnum fundEnum = FundEnum.findByFundCode(fundValue.getFund());

            fundValue.setName(fundEnum.fundName());
            fundValue.setCategory(fundEnum.category());
        }

        // Calculate current value of each fund
        List<SumOfQuantityByFund> sumOfQuantityList = this.fundTxnrepo.getSumOfQuantityGroupByFund();
        //List<FundNav> recentFundNavList = this.fundNavRepo.findTop6ByOrderByDateDesc();

        Double currentValue = 0D;
        for (SumOfQuantityByFund sumOfQuantity : sumOfQuantityList) {
            //for (FundNav fundNav : recentFundNavList) {
            for (FundEnum fund : FundEnum.values()) {
                //if (sumOfQuantity.getFund().equals(fund.getFund())) {
                if (sumOfQuantity.getFund().equals(fund.fundCode())) {
                    for (FundValue fundValue : fundValueList) {
                        if (sumOfQuantity.getFund().equals(fundValue.getFund())) {
                            //fundValue.setActualValue(sumOfQuantity.getSum() * fundNav.getNav());
                            fundValue.setActualValue(sumOfQuantity.getSum() * fund.nav());

                            FundEnum fundEnum = FundEnum.findByFundCode(fundValue.getFund());
                            fundValue.setName(fundEnum.fundShortName());
                            fundValue.setCategory(fundEnum.category());
                        }
                    }
                }
            }
        }

        PortfolioValue portfolioValue = new PortfolioValue();
        Double actualValue = 0D;
        Double purchaseValue = 0D;
        for (FundValue fundValue : fundValueList) {
            Double gainLoss = fundValue.getActualValue() - fundValue.getPurchaseValue();
            Double returns = gainLoss / fundValue.getPurchaseValue() * 100;

            PortfolioValue fundPortfolioValue = new PortfolioValue();
            fundPortfolioValue.setId(fundValue.getFund());
            fundPortfolioValue.setName(fundValue.getName());
            fundPortfolioValue.setValue(fundValue.getActualValue());
            fundPortfolioValue.setGainLoss(gainLoss);
            fundPortfolioValue.setReturns(returns);
            portfolioValue.getFundList().add(fundPortfolioValue);

            actualValue += fundValue.getActualValue();
            purchaseValue += fundValue.getPurchaseValue();
        }

        portfolioValue.setValue(actualValue);
        portfolioValue.setGainLoss(actualValue - purchaseValue);
        portfolioValue.setReturns((actualValue - purchaseValue) / purchaseValue * 100);

        return portfolioValue;
    }

    @Override
    public Fund viewFund(Long id) {
        Fund fund = new Fund();

        FundEnum fundEnum = FundEnum.findByFundCode(id);
        //List<FundNav> recentFundNavList = this.fundNavRepo.findTop6ByOrderByDateDesc();
        //for (FundNav fundNav : recentFundNavList) {
            //if (fundNav.getFund().equals(fundEnum.fundCode())) {
                fund.setId(fundEnum.fundCode());
                fund.setName(fundEnum.fundName());
                fund.setShortName(fundEnum.fundShortName());
                //fund.setDate(fundNav.getDate() / 1000);
                fund.setDate((new Date().getTime()) / 1000);
                fund.setNav(fundEnum.nav());

                //break;
            //}
        //}

        Double purchaseValue = 0D;
        Long day = 24 * 60 * 60L;
        List<Double> priceList = new ArrayList<Double>();
        List<Long> dateList = new ArrayList<Long>();

        List<FundTransaction> fundTxnList = this.fundTxnrepo.findByFundOrderByDateDesc(id);
        for (FundTransaction txn : fundTxnList) {
            FundTxn fundTxn = new FundTxn();
            fundTxn.setDate(txn.getDate());
            fundTxn.setNav(txn.getNav());
            fundTxn.setUnits(txn.getQuantity());
            fundTxn.setPrice(txn.getPurchaseValue());
            fundTxn.setValue(txn.getQuantity() * fund.getNav());
            fundTxn.setGainLoss(fundTxn.getValue() - txn.getPurchaseValue());
            fundTxn.setReturns((fundTxn.getGainLoss() / txn.getPurchaseValue()) * 100);
            fund.getTransactions().add(fundTxn);

            priceList.add(txn.getPurchaseValue());
            dateList.add((txn.getDate() - fund.getDate()) / day);

            fund.setValue(fund.getValue() + fundTxn.getValue());
            purchaseValue += txn.getPurchaseValue();
        }

        fund.setGainLoss(fund.getValue() - purchaseValue);

        // Calculate annualised return
        priceList.add(-fund.getValue());
        dateList.add(0L);

        Double annualisedReturn = getAnnualisedReturn(priceList, dateList) * 10000;
        annualisedReturn = (double) Math.round(annualisedReturn) / 100.0;
        fund.setReturns(annualisedReturn);

        return fund;
    }

    private static Double getAnnualisedReturn(List<Double> priceArray, List<Long> dateArray) {
        Double lastReturn = getReturn(priceArray, dateArray, 0D);

        Double incrementBy = (lastReturn < 0) ? 0.0001 : -0.0001;
        Double guess = incrementBy;

        do {
            Double _return = getReturn(priceArray, dateArray, guess);

            if ((_return < 0 && lastReturn > 0) || (_return > 0 && lastReturn < 0))
                return guess;

            guess += incrementBy;
            lastReturn = _return;
        } while(guess != 1.0D || guess != -1.0D);

        return 0D;
    }

    private static Double getReturn(List<Double> priceArray, List<Long> dateArray, Double guess) {
        Double _return = 0D;
        for (int index = 0; index < priceArray.size(); index++) {
            Double numerator = priceArray.get(index);

            Double exponent = (dateArray.get(index) - dateArray.get(dateArray.size() - 1)) / 365.0D;
            Double denominator = Math.pow((1 + guess), exponent);

            _return += numerator / denominator;
        }
        return _return;
    }

    @Override
    public List<FundValue> getAllFundsValue() {
        LOGGER.info("Inside getAllFundsValue()");

        PortfolioValue portfolioValue = new PortfolioValue();

        // Calculate purchase value of each fund
        List<FundValue> fundValueList = this.fundTxnrepo.getSumOfPurchaseValueGroupByFund();
        for (FundValue fundValue : fundValueList) {
            FundEnum fundEnum = FundEnum.findByFundCode(fundValue.getFund());

            fundValue.setName(fundEnum.fundName());
            fundValue.setCategory(fundEnum.category());
        }

        // Calculate current value of each fund
        List<SumOfQuantityByFund> sumOfQuantityList = this.fundTxnrepo.getSumOfQuantityGroupByFund();
        //List<FundNav> recentFundNavList = this.fundNavRepo.findTop6ByOrderByDateDesc();

        Double currentValue = 0D;
        for (SumOfQuantityByFund sumOfQuantity : sumOfQuantityList) {
            //for (FundNav fundNav : recentFundNavList) {
            for (FundEnum fund : FundEnum.values()) {
                //if (sumOfQuantity.getFund().equals(fundNav.getFund())) {
                if (sumOfQuantity.getFund().equals(fund.fundCode())) {
                    for (FundValue fundValue : fundValueList) {
                        if (sumOfQuantity.getFund().equals(fundValue.getFund())) {
                            //fundValue.setActualValue(sumOfQuantity.getSum() * fundNav.getNav());
                            fundValue.setActualValue(sumOfQuantity.getSum() * fund.nav());
                        }
                    }
                }
            }
        }

        return fundValueList;
    }

    @Override
    public List<FundView> listFunds() {
        LOGGER.info("Inside listFunds()");

        List<FundView> fundViewList = new ArrayList<FundView>();
        for (FundEnum fund : FundEnum.values()) {
            FundView fundView = new FundView();
            fundView.setName(fund.fundName());

            //List<FundNav> recentFundNavList = this.fundNavRepo.findTop6ByOrderByDateDesc();
            List<FundTransaction> fundTxnList = this.fundTxnrepo.findByFundOrderByDateDesc(fund.fundCode());
            //for (FundNav fundNav : recentFundNavList) {
                //if (fund.fundCode().equals(fundNav.getFund())) {
                if (fund.fundCode().equals(fund.fundCode())) {
                    for (FundTransaction fundTxn : fundTxnList) {
                        fundView.setQuantity(fundView.getQuantity() + fundTxn.getQuantity());
                        fundView.setPurchaseValue(fundView.getPurchaseValue() + fundTxn.getPurchaseValue());
                        fundView.setAverageNav(fundView.getAverageNav() + fundTxn.getNav());
                    }
                    fundView.setAverageNav(fundView.getAverageNav() / fundTxnList.size());

                    //fundView.setNav(fundNav.getNav());
                    //fundView.setActualValue(fundView.getQuantity() * fundNav.getNav());
                    fundView.setNav(fund.nav());
                    fundView.setActualValue(fundView.getQuantity() * fund.nav());

                    fundView.setChangeInValue(fundView.getActualValue() - fundView.getPurchaseValue());
                    fundView.setRealisedReturn((fundView.getChangeInValue() / fundView.getPurchaseValue()) * 100);
                }
            //}
            fundViewList.add(fundView);
        }

        return fundViewList;
    }
}
