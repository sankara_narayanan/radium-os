package org.openshift.radium.dto;

/**
 * Created by sankaranarayanan on 7/20/17.
 */
public class OverdraftTransactionDto {
    private Double amount;

    private Long date;

    private String comment;

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
