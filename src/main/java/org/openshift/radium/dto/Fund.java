package org.openshift.radium.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sankaranarayanan on 5/13/18.
 */
public class Fund {
    private Long id;

    private String name;

    private String shortName;

    private Long date;

    private Double nav;

    private Double value = 0D;

    private Double gainLoss;

    private Double returns;

    private Double expectedReturns;

    private List<FundTxn> transactions = new ArrayList<FundTxn>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public Double getNav() {
        return nav;
    }

    public void setNav(Double nav) {
        this.nav = nav;
    }

    public List<FundTxn> getTransactions() {
        return transactions;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public Double getGainLoss() {
        return gainLoss;
    }

    public void setGainLoss(Double gainLoss) {
        this.gainLoss = gainLoss;
    }

    public Double getReturns() {
        return returns;
    }

    public void setReturns(Double returns) {
        this.returns = returns;
    }

    public Double getExpectedReturns() {
        return expectedReturns;
    }

    public void setExpectedReturns(Double expectedReturns) {
        this.expectedReturns = expectedReturns;
    }
}
