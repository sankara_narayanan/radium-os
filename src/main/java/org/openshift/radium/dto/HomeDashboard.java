package org.openshift.radium.dto;

import java.util.ArrayList;
import java.util.List;

public class HomeDashboard {
	private List<ExpenseItem> expenses;

	private List<RunItem> runs;
	
	public List<ExpenseItem> getExpenses() {
		if (expenses == null) {
			expenses = new ArrayList<ExpenseItem>();
		}
		return expenses;
	}

	public void setExpenses(List<ExpenseItem> expenses) {
		this.expenses = expenses;
	}

	public List<RunItem> getRuns() {
		if (runs == null) {
			runs = new ArrayList<RunItem>();
		}
		return runs;
	}

	public void setRuns(List<RunItem> runs) {
		this.runs = runs;
	}
}
