package org.openshift.radium.dto;

public class ExpenseItem {
	private String month;
	
	private Double expense;

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public Double getExpense() {
		return expense;
	}

	public void setExpense(Double expense) {
		this.expense = expense;
	}
}
