package org.openshift.radium.dto;

/**
 * Created by sankaranarayanan on 6/4/17.
 */
public class FundView {
    private String name;

    private Double quantity = 0D;

    private Double averageNav = 0D;

    private Double nav = 0D;

    private Double purchaseValue = 0D;

    private Double actualValue = 0D;

    private Double changeInValue = 0D;

    private Double realisedReturn = 0D;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public Double getAverageNav() {
        return averageNav;
    }

    public void setAverageNav(Double averageNav) {
        this.averageNav = averageNav;
    }

    public Double getNav() {
        return nav;
    }

    public void setNav(Double nav) {
        this.nav = nav;
    }

    public Double getPurchaseValue() {
        return purchaseValue;
    }

    public void setPurchaseValue(Double purchaseValue) {
        this.purchaseValue = purchaseValue;
    }

    public Double getActualValue() {
        return actualValue;
    }

    public void setActualValue(Double actualValue) {
        this.actualValue = actualValue;
    }

    public Double getChangeInValue() {
        return changeInValue;
    }

    public void setChangeInValue(Double changeInValue) {
        this.changeInValue = changeInValue;
    }

    public Double getRealisedReturn() {
        return realisedReturn;
    }

    public void setRealisedReturn(Double realisedReturn) {
        this.realisedReturn = realisedReturn;
    }
}
