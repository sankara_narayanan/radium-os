package org.openshift.radium.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sankaranarayanan on 6/30/18.
 */
public class HabitMonthLog {
    private Long habitId;

    private String name;

    private List<Integer> daysCommitted = new ArrayList<Integer>();

    private List<Integer> daysCompleted = new ArrayList<Integer>();

    private List<Integer> daysUncompleted = new ArrayList<Integer>();

    private List<Integer> daysPending = new ArrayList<Integer>();

    public Long getHabitId() {
        return habitId;
    }

    public void setHabitId(Long habitId) {
        this.habitId = habitId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Integer> getDaysCommitted() {
        return daysCommitted;
    }

    public List<Integer> getDaysCompleted() {
        return daysCompleted;
    }

    public List<Integer> getDaysUncompleted() {
        return daysUncompleted;
    }

    public List<Integer> getDaysPending() {
        return daysPending;
    }
}
