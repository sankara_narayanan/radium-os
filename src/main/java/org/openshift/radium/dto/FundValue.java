package org.openshift.radium.dto;

/**
 * Created by sankaranarayanan on 6/3/17.
 */
public class FundValue {
    private Long fund;

    private String name;

    private String category;

    private Double purchaseValue;

    private Double actualValue;

    public FundValue(Long fund, Double purchaseValue) {
        this.fund = fund;
        this.purchaseValue = purchaseValue;
    }

    public Long getFund() {
        return fund;
    }

    public void setFund(Long fund) {
        this.fund = fund;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Double getPurchaseValue() {
        return purchaseValue;
    }

    public void setPurchaseValue(Double purchaseValue) {
        this.purchaseValue = purchaseValue;
    }

    public Double getActualValue() {
        return actualValue;
    }

    public void setActualValue(Double actualValue) {
        this.actualValue = actualValue;
    }
}
