package org.openshift.radium.dto;

/**
 * Created by sankaranarayanan on 4/26/17.
 */
public class SumOfQuantityByFund {
    public Long fund;

    public Double sum;

    public SumOfQuantityByFund(Long fund, Double sum) {
        this.fund = fund;
        this.sum = sum;
    }

    public Long getFund() {
        return fund;
    }

    public void setFund(Long fund) {
        this.fund = fund;
    }

    public Double getSum() {
        return sum;
    }

    public void setSum(Double sum) {
        this.sum = sum;
    }
}
