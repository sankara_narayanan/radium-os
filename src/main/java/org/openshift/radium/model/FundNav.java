package org.openshift.radium.model;

import javax.persistence.*;

/**
 * Created by sankaranarayanan on 4/8/17.
 */
@Entity
@Table(name = "FUND_NAV")
public class FundNav {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "DATE")
    private Long date;

    @Column(name = "NAV")
    private Double nav;

    @Column(name = "FUND")
    private Long fund;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public Double getNav() {
        return nav;
    }

    public void setNav(Double nav) {
        this.nav = nav;
    }

    public Long getFund() {
        return fund;
    }

    public void setFund(Long fund) {
        this.fund = fund;
    }
}
