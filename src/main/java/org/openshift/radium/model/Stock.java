package org.openshift.radium.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by sankaranarayanan on 3/9/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Stock implements Comparable<Stock> {
    @JsonCreator
    public Stock(@JsonProperty("t") String code,
                 @JsonProperty("e") String market,
                 @JsonProperty("l_fix") Double price,
                 @JsonProperty("c") String change,
                 @JsonProperty("cp") String changeInPercent,
                 @JsonProperty("lt_dts") String ltd) {
        this.symbol = code;
        this.exchange = market;
        this.price = price;
        this.change = change;
        this.changeInPercent = changeInPercent;
        this.ltd = ltd;
    }

    private String name;

    private String symbol;

    private String exchange;

    private Double price;

    private String change;

    private String changeInPercent;

    private String ltd;

    private Date lastTradedDate;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getChange() {
        return change;
    }

    public void setChange(String change) {
        this.change = change;
    }

    public String getChangeInPercent() {
        return changeInPercent;
    }

    public void setChangeInPercent(String changeInPercent) {
        this.changeInPercent = changeInPercent;
    }

    public String getLtd() {
        return ltd;
    }

    public void setLtd(String ltd) {
        this.ltd = ltd;
    }

    public Date getLastTradedDate() {
        if (lastTradedDate == null) {
            try {
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                this.lastTradedDate = dateFormat.parse(ltd.split("T")[0]);
            } catch(Exception ex) {
                ex.printStackTrace();
            }
        }
        return lastTradedDate;
    }

    public void setLastTradedDate(Date lastTradedDate) {
        this.lastTradedDate = lastTradedDate;
    }

    @Override
    public int compareTo(Stock o) {
        Double thisValue = Double.valueOf(this.changeInPercent);
        Double paramValue = Double.valueOf(o.changeInPercent);
        if (thisValue > paramValue)
            return 1;
        else if (thisValue == paramValue)
            return 0;
        else
            return -1;
    }
}
