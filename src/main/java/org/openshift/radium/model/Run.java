package org.openshift.radium.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedNativeQuery;
import javax.persistence.Table;

@Entity
@Table(name = "RUNS")
@NamedNativeQuery(name="Run.findSumOfDistanceGroupByMonth",
query="select to_char(to_timestamp(run_date / 1000), 'MM') as mm, to_char(to_timestamp(run_date / 1000), 'Mon') as month, sum(distance) as distance  from runs group by 1, 2 order by 1 desc")
public class Run {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "RUN_DATE")
	private Long runDate;
	
	@Column(name = "DURATION")
	private Integer duration;

	@Column(name = "DISTANCE")
	private Double distance;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getRunDate() {
		return runDate;
	}

	public void setRunDate(Long runDate) {
		this.runDate = runDate;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public Double getDistance() {
		return distance;
	}

	public void setDistance(Double distance) {
		this.distance = distance;
	}	
}
