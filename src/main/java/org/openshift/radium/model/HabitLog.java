package org.openshift.radium.model;

import javax.persistence.*;

/**
 * Created by sankaranarayanan on 7/20/17.
 */
@Entity
@Table(name = "HABIT_LOGS")
public class HabitLog {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "HABIT_FK")
    private Habit habit;

    @Column(name = "DATE")
    private Long date;

    @Column(name = "DONE")
    private Boolean done;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Habit getHabit() {
        return habit;
    }

    public void setHabit(Habit habit) {
        this.habit = habit;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public Boolean getDone() {
        return done;
    }

    public void setDone(Boolean done) {
        this.done = done;
    }
}
