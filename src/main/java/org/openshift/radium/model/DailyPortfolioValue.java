package org.openshift.radium.model;

import javax.persistence.*;

/**
 * Created by sankaranarayanan on 6/2/17.
 */
@Entity
@Table(name = "daily_portfolio_value")
public class DailyPortfolioValue {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "DATE")
    private Long date;

    @Column(name = "CURRENT_VALUE")
    private Double currentValue;

    @Column(name = "INVESTED_VALUE")
    private Double investedValue;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public Double getCurrentValue() {
        return currentValue;
    }

    public void setCurrentValue(Double currentValue) {
        this.currentValue = currentValue;
    }

    public Double getInvestedValue() {
        return investedValue;
    }

    public void setInvestedValue(Double investedValue) {
        this.investedValue = investedValue;
    }
}
