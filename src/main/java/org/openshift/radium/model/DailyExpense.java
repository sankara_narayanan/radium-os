package org.openshift.radium.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "DAILY_EXPENSES")
@NamedNativeQuery(name="DailyExpense.findSumOfExpenseGroupByMonth", query="select to_char(to_timestamp(expense_date / 1000), 'MM') as mm, to_char(to_timestamp(expense_date / 1000), 'Mon') as month, sum(cost) as expense from daily_expenses group by 1, 2 order by 1 desc")
@NamedQuery(name="DailyExpense.findSumOfExpenseGroupByItem", query="select category, item, sum(cost) from DailyExpense where expenseDate between :startTime and :endTime group by category, item order by 3 desc")
public class DailyExpense {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "CATEGORY")
	private String category;

	@Column(name = "ITEM")
	private String item;

	@Column(name = "COST")
	private Double cost;

	@Column(name = "EXPENSE_DATE")
	private Long expenseDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	public Double getCost() {
		return cost;
	}

	public void setCost(Double cost) {
		this.cost = cost;
	}

	public Long getExpenseDate() {
		return expenseDate;
	}

	public void setExpenseDate(Long expenseDate) {
		this.expenseDate = expenseDate;
	}
}
