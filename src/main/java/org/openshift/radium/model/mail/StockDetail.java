package org.openshift.radium.model.mail;

/**
 * Created by sankaranarayanan on 3/13/17.
 */
public class StockDetail {
    private String name;

    private String symbol;

    private Double price;

    private Double changeInPercentage;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getChangeInPercentage() {
        return changeInPercentage;
    }

    public void setChangeInPercentage(Double changeInPercentage) {
        this.changeInPercentage = changeInPercentage;
    }
}
