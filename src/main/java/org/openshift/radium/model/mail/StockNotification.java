package org.openshift.radium.model.mail;

import org.openshift.radium.model.Stock;

import java.util.List;

/**
 * Created by sankaranarayanan on 3/13/17.
 */
public class StockNotification {
    private List<Stock> watchList;

    public List<Stock> getWatchList() {
        return watchList;
    }

    public void setWatchList(List<Stock> watchList) {
        this.watchList = watchList;
    }
}
