package org.openshift.radium.model;

import javax.persistence.*;

/**
 * Created by sankaranarayanan on 4/8/17.
 */
@Entity
@Table(name = "FUND_TRANSACTIONS")
public class FundTransaction {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "FUND")
    private Long fund;

    @Column(name = "DATE")
    private Long date;

    @Column(name = "NAV")
    private Double nav;

    @Column(name = "QUANTITY")
    private Double quantity;

    @Column(name = "PURCHASE_VALUE")
    private Double purchaseValue;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getFund() {
        return fund;
    }

    public void setFund(Long fund) {
        this.fund = fund;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public Double getNav() {
        return nav;
    }

    public void setNav(Double nav) {
        this.nav = nav;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public Double getPurchaseValue() {
        return purchaseValue;
    }

    public void setPurchaseValue(Double purchaseValue) {
        this.purchaseValue = purchaseValue;
    }
}
