package org.openshift.radium.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by sankaranarayanan on 4/19/17.
 */
@Entity
@Table(name = "CATEGORY_ITEM")
public class CategoryItem {
    @Id
    private Long id;

    @Column(name = "CATEGORY")
    private String category;

    @Column(name = "ITEM")
    private String item;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }
}
