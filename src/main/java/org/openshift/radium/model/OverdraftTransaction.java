package org.openshift.radium.model;

import javax.persistence.*;

/**
 * Created by sankaranarayanan on 7/20/17.
 */
@Entity
@Table(name = "OVERDRAFT_TRANSACTIONS")
public class OverdraftTransaction {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "DATE")
    private Long date;

    @Column(name = "DEBIT")
    private Double debit;

    @Column(name = "CREDIT")
    private Double credit;

    @Column(name = "COMMENT")
    private String comment;

    @Column(name = "BOOK_BALANCE")
    private Double bookBalance;

    @Column(name = "AVAILABLE_BALANCE")
    private Double availableBalance;

    @Column(name = "_LIMIT")
    private Double limit;

    @Column(name = "DRAWING_POWER")
    private Double drawingPower;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public Double getDebit() {
        return debit;
    }

    public void setDebit(Double debit) {
        this.debit = debit;
    }

    public Double getCredit() {
        return credit;
    }

    public void setCredit(Double credit) {
        this.credit = credit;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Double getBookBalance() {
        return bookBalance;
    }

    public void setBookBalance(Double bookBalance) {
        this.bookBalance = bookBalance;
    }

    public Double getAvailableBalance() {
        return availableBalance;
    }

    public void setAvailableBalance(Double availableBalance) {
        this.availableBalance = availableBalance;
    }

    public Double getLimit() {
        return limit;
    }

    public void setLimit(Double limit) {
        this.limit = limit;
    }

    public Double getDrawingPower() {
        return drawingPower;
    }

    public void setDrawingPower(Double drawingPower) {
        this.drawingPower = drawingPower;
    }
}
