package org.openshift.radium.model;

import javax.persistence.*;

/**
 * Created by sankaranarayanan on 7/20/17.
 */
@Entity
@Table(name = "OVERDRAFT_ACCOUNTS")
public class OverdraftAccount {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "BOOK_BALANCE")
    private Double bookBalance;

    @Column(name = "AVAILABLE_BALANCE")
    private Double availableBalance;

    @Column(name = "_LIMIT")
    private Double limit;

    @Column(name = "DRAWING_POWER")
    private Double drawingPower;

    @Column(name = "INTEREST_RATE")
    private Double interestRate;

    @Transient
    private Double interest;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getBookBalance() {
        return bookBalance;
    }

    public void setBookBalance(Double bookBalance) {
        this.bookBalance = bookBalance;
    }

    public Double getAvailableBalance() {
        return availableBalance;
    }

    public void setAvailableBalance(Double availableBalance) {
        this.availableBalance = availableBalance;
    }

    public Double getLimit() {
        return limit;
    }

    public void setLimit(Double limit) {
        this.limit = limit;
    }

    public Double getDrawingPower() {
        return drawingPower;
    }

    public void setDrawingPower(Double drawingPower) {
        this.drawingPower = drawingPower;
    }

    public Double getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(Double interestRate) {
        this.interestRate = interestRate;
    }

    public Double getInterest() {
        return interest;
    }

    public void setInterest(Double interest) {
        this.interest = interest;
    }
}
