package org.openshift.radium.model;

import javax.persistence.*;

/**
 * Created by sankaranarayanan on 6/4/17.
 */
@Entity
@Table(name = "BILL_REMINDER")
public class BillReminder {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "CATEGORY")
    private String category;

    @Column(name = "ITEM")
    private String item;

    @Column(name = "COST")
    private Double cost;

    @Column(name = "NEXT_REMINDER")
    private Long nextReminder;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public Long getNextReminder() {
        return nextReminder;
    }

    public void setNextReminder(Long nextReminder) {
        this.nextReminder = nextReminder;
    }
}
