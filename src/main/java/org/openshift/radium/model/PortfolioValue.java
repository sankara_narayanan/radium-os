package org.openshift.radium.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sankaranarayanan on 4/26/17.
 */
public class PortfolioValue {
    private Long id;

    private String name;

    private Double value;

    private Double gainLoss;

    private Double returns;

    private List<PortfolioValue> fundList = new ArrayList<PortfolioValue>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public Double getGainLoss() {
        return gainLoss;
    }

    public void setGainLoss(Double gainLoss) {
        this.gainLoss = gainLoss;
    }

    public Double getReturns() {
        return returns;
    }

    public void setReturns(Double returns) {
        this.returns = returns;
    }

    public List<PortfolioValue> getFundList() {
        return fundList;
    }
}
