package org.openshift.radium.enums;

/**
 * Created by sankaranarayanan on 5/6/17.
 */
public enum BillReminderTaskEnum {
    GET_BILL_NAME,
    GET_BILL_AMOUNT,
    GET_BILL_DUE_DATE,
    IS_RECURRENT_BILL;
}
