package org.openshift.radium.enums;

/**
 * Created by sankaranarayanan on 4/4/17.
 */
public enum FundEnum {
    FI_SMALLER_COMPANIES(27L, 118525L, "Franklin India Smaller Companies", "FI Smaller Companies", "Smallcap"),
    MA_EMERGING_BLUECHIP(45L, 118834L, "Mirae Asset Emerging Bluechip", "MA Emerging Bluechip", "Midcap"),
    FI_HIGHGROWTH_COMPANIES(27L, 118564L, "Franklin India High Growth Companies", "FI High Growth Companies", "Multicap"),
    PP_LONG_TERM_EQUITY(64L, 122639L, "Parag Parikh Long Term Equity", "PP Long Term Equity", "Multicap"),
    FI_TAX_SHIELD(27L, 118540L, "Franklin India TaxShield", "FI Taxshield", "ELSS"),
    AX_LONG_TERM_EQUITY(53L, 120503L, "Axis Long Term Equity", "Axis Long Term Equity", "ELSS");

    Long fundHouse;

    Long fundCode;

    Double nav;

    String fundName;

    String fundShortName;

    String category;

    FundEnum(Long fundHouse, Long fundCode, String fundName, String fundShortName, String category) {
        this.fundHouse = fundHouse;
        this.fundCode = fundCode;
        this.fundName = fundName;
        this.fundShortName = fundShortName;
        this.category = category;
    }

    public Long fundHouse() {
        return this.fundHouse;
    }

    public Long fundCode() {
        return this.fundCode;
    }

    public Double nav() {
        return nav;
    }

    public void setNav(Double nav) {
        this.nav = nav;
    }

    public String fundName() {
        return this.fundName;
    }

    public String fundShortName() {
        return this.fundShortName;
    }

    public String category() {
        return this.category;
    }

    public static FundEnum findByFundCode(Long fundCode) {
        for (FundEnum fundEnum : FundEnum.values()) {
            if (fundEnum.fundCode().equals(fundCode)) {
                return fundEnum;
            }
        }
        return null;
    }
}
