package org.openshift.radium.enums;

/**
 * Created by sankaranarayanan on 3/16/17.
 */
public enum StockEnum {
    AXISBANK("Axis Bank Ltd."),
    BANKBARODA("Bank of Baroda"),
    BANKINDIA("Bank of India"),
    CANBK("Canara Bank"),
    FEDERALBNK("Federal Bank Ltd."),
    HDFCBANK("HDFC Bank Ltd."),
    ICICIBANK("ICICI Bank Ltd."),
    INDUSINDBK("IndusInd Bank Ltd."),
    KOTAKBANK("Kotak Mahindra Bank Ltd."),
    PNB("Punjab National Bank"),
    SBIN("State Bank of India"),
    YESBANK("Yes Bank Ltd.");

    private String name;

    StockEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
}
