package org.openshift.radium.client.stock.google;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.openshift.radium.enums.StockEnum;
import org.openshift.radium.model.Stock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * Created by sankaranarayanan on 3/16/17.
 */
@Component
public class GoogleFinanceClient {

    @Value("${stock.google.finance.api}")
    private String googleApiUrl;

    @Autowired
    private RestTemplate googleApi;

    @Autowired
    private ObjectMapper objectMapper;

    public Stock getStock(String exchange, String symbol) throws Exception {
        String stockQuote = this.googleApi.getForObject(String.format(googleApiUrl, exchange + ":" + symbol), String.class);

        stockQuote = stockQuote.replace("//", "");
        List<Stock> stockList = objectMapper.readValue(stockQuote, new TypeReference<List<Stock>>() {});
        Stock stock = stockList.get(0);

        // Initializing stock object
        StockEnum stockEnum = StockEnum.valueOf(stock.getSymbol());
        stock.setName(stockEnum.getName());
        stock.getLastTradedDate();

        return stock;
    }
}
