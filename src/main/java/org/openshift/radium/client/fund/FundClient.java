package org.openshift.radium.client.fund;

import org.openshift.radium.enums.FundEnum;
import org.openshift.radium.model.FundNav;
import org.openshift.radium.service.api.IFundService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by sankaranarayanan on 4/4/17.
 */
@Component
public class FundClient {
    private static final Logger LOGGER = LoggerFactory.getLogger(FundClient.class);

    @Autowired
    private RestTemplate fundApi;

    @Autowired
    private IFundService fundService;

    @Value("${fund.date.nav.url}")
    private String fundDateNavUrl;

    @Value("${fund.date.nav.index}")
    private int fundDateNavIndex;

    @Value("${fund.nav.url}")
    private String fundNavUrl;

    @Value("${fund.nav.index}")
    private int navIndex;

    @Value("${fund.date.index}")
    private int dateIndex;

    public Double pullNav(FundEnum fund, Long date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
        dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));

        String strDate = dateFormat.format(new Date(date));
        String url = String.format(fundDateNavUrl, fund.fundHouse(), strDate, strDate);
        String fundNavTxt = this.fundApi.getForObject(url, String.class);
        String[] fundNavArray = fundNavTxt.split("\\r?\\n");

        for (String fundDetails : fundNavArray) {
            if (fundDetails.startsWith(fund.fundCode().toString())) {
                String[] fundDetailArray = fundDetails.split(";");
                return Double.valueOf(fundDetailArray[fundDateNavIndex]);
            }
        }

        return null;
    }

    @PostConstruct
    public void pullNav() {
        String fundNavTxt = this.fundApi.getForObject(fundNavUrl, String.class);
        String[] fundNavArray = fundNavTxt.split("\\r?\\n");

        for (FundEnum fundEnum : FundEnum.values()) {
            for (String fundDetails : fundNavArray) {
                if (fundDetails.startsWith(fundEnum.fundCode().toString())) {
                    String[] fundDetailArray = fundDetails.split(";");
                    fundEnum.setNav(Double.valueOf(fundDetailArray[navIndex]));
                }
            }
        }
    }

    public void saveDailyNav() throws Exception {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
        dateFormat.setTimeZone(TimeZone.getTimeZone("IST"));

        String fundNavTxt = this.fundApi.getForObject(fundNavUrl, String.class);
        String[] fundNavArray = fundNavTxt.split("\\r?\\n");

        for (FundEnum fundEnum : FundEnum.values()) {
            for (String fundDetails : fundNavArray) {
                if (fundDetails.startsWith(fundEnum.fundCode().toString())) {
                    String[] fundDetailArray = fundDetails.split(";");

                    Double nav = Double.valueOf(fundDetailArray[navIndex]);
                    Long fund = Long.valueOf(fundEnum.fundCode());
                    Date date = dateFormat.parse(fundDetailArray[dateIndex]);

                    LOGGER.info("Fund name: {}", fund);
                    LOGGER.info("Fund NAV: {}", nav);
                    LOGGER.info("Fund date: {}", date);

                    FundNav fundNav = new FundNav();
                    fundNav.setDate(date.getTime());
                    fundNav.setNav(nav);
                    fundNav.setFund(fund);
                    this.fundService.saveDailyNav(fundNav);
                }
            }
        }

        this.fundService.saveDailyPortfolioValue();
    }
}
