package org.openshift.radium.repo.api;

import org.openshift.radium.model.FundNav;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IFundNavRepository extends IBaseRepository<FundNav, Long>{
    FundNav findByFundAndDate(Long fund, Long date);

    List<FundNav> findTop6ByOrderByDateDesc();
}
