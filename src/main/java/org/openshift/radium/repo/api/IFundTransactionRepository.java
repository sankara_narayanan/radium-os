package org.openshift.radium.repo.api;

import org.openshift.radium.dto.FundValue;
import org.openshift.radium.dto.SumOfQuantityByFund;
import org.openshift.radium.model.FundTransaction;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by sankaranarayanan on 4/8/17.
 */
@Repository
public interface IFundTransactionRepository extends IBaseRepository<FundTransaction, Long>{
    List<FundTransaction> findByFundOrderByDateDesc(Long fund);

    List<FundTransaction> findTop6ByFundOrderByDateDesc(Long fund);

    @Query("SELECT SUM(purchaseValue) FROM org.openshift.radium.model.FundTransaction")
    Double getTotalPurchaseValue();

    @Query("SELECT new org.openshift.radium.dto.FundValue(fund AS fund, SUM(purchaseValue) AS purchaseValue) FROM org.openshift.radium.model.FundTransaction GROUP BY fund")
    List<FundValue> getSumOfPurchaseValueGroupByFund();

    @Query("SELECT new org.openshift.radium.dto.SumOfQuantityByFund(fund AS fund, SUM(quantity) AS sum) FROM org.openshift.radium.model.FundTransaction GROUP BY fund")
    List<SumOfQuantityByFund> getSumOfQuantityGroupByFund();
}
