package org.openshift.radium.repo.api;

import org.openshift.radium.model.OverdraftAccount;
import org.springframework.stereotype.Repository;

/**
 * Created by sankaranarayanan on 7/20/17.
 */
@Repository
public interface IOverdraftAccountRepository extends IBaseRepository<OverdraftAccount, Long>{
}
