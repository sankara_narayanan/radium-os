package org.openshift.radium.repo.api;

import org.openshift.radium.model.Habit;
import org.openshift.radium.model.HabitLog;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IHabitLogRepository extends IBaseRepository<HabitLog, Long> {
    HabitLog findByHabitAndDate(Habit habit, Long date);

    List<HabitLog> findByHabitAndDateBetween(Habit habit, Long startDate, Long endDate);
}
