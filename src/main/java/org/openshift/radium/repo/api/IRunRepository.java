package org.openshift.radium.repo.api;

import java.util.List;

import org.springframework.stereotype.Repository;

import org.openshift.radium.model.Run;

@Repository
public interface IRunRepository extends IBaseRepository<Run, Long>{
	List<Run> findByRunDateBetweenOrderByRunDateDesc(Long startTime, Long endTime);
	
	List<Object[]> findSumOfDistanceGroupByMonth();
}
