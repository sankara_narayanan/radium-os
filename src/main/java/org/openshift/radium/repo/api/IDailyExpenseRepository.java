package org.openshift.radium.repo.api;

import org.openshift.radium.model.DailyExpense;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IDailyExpenseRepository extends IBaseRepository<DailyExpense, Long> {
	List<DailyExpense> findByExpenseDateBetween(Long startTime, Long endTime);
	
	List<Object[]> findSumOfExpenseGroupByMonth();
	
	List<Object[]> findSumOfExpenseGroupByItem(@Param("startTime") Long startTime, @Param("endTime") Long endTime);

	@Query("SELECT expenseDate, SUM(cost) FROM DailyExpense WHERE expenseDate BETWEEN :startTime AND :endTime GROUP BY expenseDate ORDER BY expenseDate")
	List<Object[]> findSumOfCostGroupByExpenseDate(@Param("startTime") Long startTime, @Param("endTime") Long endTime);
}
