package org.openshift.radium.repo.api;

import org.openshift.radium.model.Habit;
import org.springframework.stereotype.Repository;

@Repository
public interface IHabitRepository extends IBaseRepository<Habit, Long> {
}
