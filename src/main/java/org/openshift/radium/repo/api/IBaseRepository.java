package org.openshift.radium.repo.api;

import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.Repository;

import java.io.Serializable;
import java.util.List;

@NoRepositoryBean
public interface IBaseRepository<T, ID extends Serializable> extends Repository<T, ID> {
	T findOne(ID id);

	List<T> findAll();
	
	T save(T persisted);
	
	void delete(T deleted);
}
