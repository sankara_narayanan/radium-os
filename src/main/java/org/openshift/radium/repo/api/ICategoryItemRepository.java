package org.openshift.radium.repo.api;

import org.openshift.radium.model.CategoryItem;
import org.springframework.stereotype.Repository;

/**
 * Created by sankaranarayanan on 4/19/17.
 */
@Repository
public interface ICategoryItemRepository extends IBaseRepository<CategoryItem, Long> {
}
