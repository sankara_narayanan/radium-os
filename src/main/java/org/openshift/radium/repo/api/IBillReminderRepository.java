package org.openshift.radium.repo.api;

import org.openshift.radium.model.BillReminder;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by sankaranarayanan on 6/4/17.
 */
@Repository
public interface IBillReminderRepository extends IBaseRepository<BillReminder, Long>{
    BillReminder findById(Long id);

    List<BillReminder> findByNextReminderNot(Long nextReminder);
}