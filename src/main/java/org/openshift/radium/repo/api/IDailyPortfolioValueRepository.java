package org.openshift.radium.repo.api;

import org.openshift.radium.model.DailyPortfolioValue;
import org.springframework.stereotype.Repository;

@Repository
public interface IDailyPortfolioValueRepository extends IBaseRepository<DailyPortfolioValue, Long> {
}
