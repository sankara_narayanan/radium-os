package org.openshift.radium.repo.api;

import org.openshift.radium.model.OverdraftTransaction;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by sankaranarayanan on 7/20/17.
 */
@Repository
public interface IOverdraftTransactionRepository extends IBaseRepository<OverdraftTransaction, Long> {
    OverdraftTransaction findTopByOrderByDateDesc();

    List<OverdraftTransaction> findByDateBetweenOrderByDateAsc(Long startDate, Long endDate);
}
