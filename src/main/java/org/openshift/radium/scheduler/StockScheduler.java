package org.openshift.radium.scheduler;

import org.openshift.radium.service.api.IStockService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Created by sankaranarayanan on 3/12/17.
 */
@Component
public class StockScheduler {
    private static final Logger LOGGER = LoggerFactory.getLogger(StockScheduler.class);

    @Autowired
    private IStockService stockService;

    @Scheduled(cron = "${stock.cron.expression}", zone = "IST")
    public void triggerValueChecker() throws Exception {
        LOGGER.info("Stock watch list notification email triggered");
        System.out.println("Testing scheduler");
        stockService.sendNotificationMail();
    }
}
