package org.openshift.radium.scheduler;

import org.openshift.radium.service.api.IOverdraftService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by sankaranarayanan on 7/21/17.
 */
@Component
public class OverdraftScheduler {
    private static final Logger LOGGER = LoggerFactory.getLogger(OverdraftScheduler.class);

    @Autowired
    private IOverdraftService odService;

    @Scheduled(cron = "${od.monthly.stmt.cron.expression}", zone = "IST")
    public void sendMonhtlyStatement() throws Exception {
        Calendar today = Calendar.getInstance();
        today.setTime(new Date());
        if (today.getActualMaximum(Calendar.DAY_OF_MONTH) == today.get(Calendar.DAY_OF_MONTH)) {
            odService.debtMonthEndInterest(today.getTimeInMillis());
        }
    }
}
