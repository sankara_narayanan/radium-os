package org.openshift.radium.scheduler;

import org.openshift.radium.client.fund.FundClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Created by sankaranarayanan on 4/8/17.
 */
@Component
public class FundScheduler {
    private static final Logger LOGGER = LoggerFactory.getLogger(FundScheduler.class);

    @Autowired
    private FundClient fundClient;

    @Scheduled(cron = "${fund.cron.expression}", zone = "IST")
    public void saveDailyNavJob() throws Exception {
        this.fundClient.saveDailyNav();
    }
}
